export const msgAuth = {//Mensagens de retornos
    "erroAoConsultarLoginDeAcesso" : "ERRO_AO_CONSULTAR_LOGIN_DE_ACESSO",
    "tokenNaoEnviado": "TOKEN_NAO_ENVIADO",
    "loginInvalido" : "LOGIN_INVALIDO",
    "tokenInvalido" : "TOKEN_INVALIDO",
    "loginhaOuSenhaInvalidos": "LOGIN_OU_SENHA_INVALIDOS",
    "erroAoValidarToken": "ERRO_AO_VALIDAR_TOKEN",
    "nenhumLoginEncontrado": "NENHUM_LOGIN_USUARIO",
    "acessoNegado" : "ACESSO_NEGADO"
};

export const msgUsuario = {
    "erroAoListarUsuarios": "ERRO_AO_LISTAR_USUARIOS",
    "nenhumUsuarioEncontrado": "NENHUM_USUARIO_ENCONTRADO",
    "erroAoLocalizarUsuario": "ERRO_AO_LOCALIZAR_USUARIO",
    "erroAoInserirUsuario": "ERRO_AO_INSERIR_USUARIO",
    "erroAoAtualizarUsuario": "ERRO_AO_ATUALIZAR_USUARIO",
    "erroAoRemoverUsuario": "ERRO_AO_REMOVER_USUARIO",
    "usuarioInseridoComSucesso": "USUARIO_INSERIDO_COM_SUCESSO",
    "usuarioAtualizadoComSucesso": "USUARIO_ATUALIZADO_COM_SUCESSO",
    "usuarioRemovidoComSucesso": "USUARIO_REMOVIDO_COM_SUCESSO",
}

export const msgOrganizacao = {
    "erroAoListarOrganizacoes": "ERRO_AO_LISTAR_ORGANIZACOES",
    "nenhumaOrganizacaoEncontrada": "NENHUMA_ORGANIZACAO_ENCONTRADA",
    "erroAoLocalizarOrganizacao": "ERRO_AO_LOCALIZAR_ORGANIZACAO",
    "erroAoObterOrganizacao": "ERRO_AO_OBTER_ORGANIZACAO",
    "erroAoInserirOrganizacao": "ERRO_AO_INSERIR_ORGANIZACAO",
    "erroAoAtualizarOrganizacao": "ERRO_AO_ATUALIZAR_ORGANIZACAO",
    "erroAoRemoverOrganizacao": "ERRO_AO_REMOVER_ORGANIZACAO",    
    "organizacaoCampoFantasiaObrigatorio": "ORGANIZACAO_CAMPO_FANTASIA_OBIRGATORIO",
    "organizacaoCampoRazaoSocialObrigatorio": "ORGANIZACAO_CAMPO_RAZAO_SOCIAL_OBRIGATORIO",
    "organizacaoCampoChaveGrupoContatoObrigatorio": "ORGANIZACAO_CAMPO_CHAVE_GRUPO_CONTATO_OBRIGATORIO",
    "organizacaoCampoChaveGrupoDocumentoObrigatorio": "ORGANIZACAO_CAMPO_CHAVE_GRUPO_DOCUMENTO_OBRIGATORIO",
    "organizacaoCampoStatusInvalido": "ORGANIZACAO_CAMPO_STATUS_INVALIDO",
    "organizacaoInseridaComSucesso": "ORGANIZACAO_INSERIDA_COM_SUCESSO",
    "organizacaoAtualizadaComSucesso": "ORGANIZACAO_ATUALIZADA_COM_SUCESSO",
    "organizacaoRemovidaComSucesso": "ORGANIZACAO_REMOVIDA_COM_SUCESSO", 
    "organizacaoDeveSerUnica": "ORGANIZACAO_DEVE_SER_UNICA"   
}

export const msgFilial = {
    "erroAoListarFiliais": "ERRO_AO_LISTAR_FILIAIS",
    "nenhumaFilialEncontrada": "NENHUMA_FILIAL_ENCONTRADA",
    "erroAoLocalizarFilial": "ERRO_AO_LOCALIZAR_FILIAL",
    "erroAoObterFilial": "ERRO_AO_OBTER_FILIAL",
    "erroAoInserirFilial": "ERRO_AO_INSERIR_FILIAL",
    "erroAoAtualizarFilial": "ERRO_AO_ATUALIZAR_FILIAL",
    "erroAoRemoverFilial": "ERRO_AO_REMOVER_FILIAL",
    "erroAoListarFilial": "ERRO_AO_LISTAR_FILIAL",
    "filialCampoFantasiaObrigatorio": "FILIAL_CAMPO_FANTASIA_OBIRGATORIO",
    "filialCampoRazaoSocialObrigatorio": "FILIAL_CAMPO_RAZAO_SOCIAL_OBRIGATORIO",
    "filialCampoChaveGrupoContatoObrigatorio": "FILIAL_CAMPO_CHAVE_GRUPO_CONTATO_OBRIGATORIO",
    "filialCampoChaveGrupoDocumentoObrigatorio": "FILIAL_CAMPO_CHAVE_GRUPO_DOCUMENTO_OBRIGATORIO",
    "filialCampoOrganizacaoObrigatorio": "FILIAL_CAMPO_ORGANIZACAO_OBRIGATORIO",
    "filialCampoStatusInvalido": "FILIAL_CAMPO_STATUS_INVALIDO",
    "filialInseridaComSucesso": "FILIAL_INSERIDA_COM_SUCESSO",
    "filialAtualizadaComSucesso": "FILIAL_ATUALIZADA_COM_SUCESSO",
    "filialRemovidaComSucesso": "FILIAL_REMOVIDA_COM_SUCESSO", 
    "filialDeveSerUnica": "FILIAL_DEVE_SER_UNICA",
    "filialAssociacaoOrganizacaoNaoEncontrada": "FILIAL_ASSOCIADO_ORGANIZACAO_NAO_ENCONTRADA"       
}

export const msgPerfil = {
    "erroAoListarPerfis": "ERRO_AO_LISTAR_PERFIS",
    "nenhumPerfilEncontrado": "NENHUM_PERFIL_ENCONTRADO",
    "erroAoObterPerfil": "ERRO_AO_OBTER_PERFIL",
    "erroAoInserirPerfil": "ERRO_AO_INSERIR_PERFIL",
    "erroAoAtualizarPerfil": "ERRO_AO_ATUALIZAR_PERFIL",
    "erroAoRemoverPerfil": "ERRO_AO_REMOVER_PERFIL",
    "perfilInseridoComSucesso": "PERFIL_INSERIDO_COM_SUCESSO",
    "perfilAtualizadoComSucesso": "PERFIL_ATUALIZADO_COM_SUCESSO",
    "perfilRemovidoComSucesso": "PERFIL_REMOVIDO_COM_SUCESSO",
    "perfilDeveSerUnico": "PERFIL_DEVE_SER_UNICO",
    "perfilCampoNomeRangeObrigatorio": "PERFIL_CAMPO_NOME_RANGE_OBRIGATORIO",
    "perfilListaAcoesObrigatorio": "PERFIL_LISTA_ACOES_OBRIGATORIO"        
}

export const msgAcao = {
    "erroAoListarAcoes": "ERRO_AO_LISTAR_ACOES",
    "nenhumAAcaoEncontrada": "NENHUMA_ACAO_ENCONTRADA",
    "erroAoLocalizarAcao": "ERRO_AO_LOCALIZAR_ACAO",
    "erroAoInserirAcao": "ERRO_AO_INSERIR_ACAO",
    "erroAoAtualizarAcao": "ERRO_AO_ATUALIZAR_ACAO",
    "erroAoRemoverAcao": "ERRO_AO_REMOVER_ACAO",
    "acaoInseridaComSucesso": "ACAO_INSERIDO_COM_SUCESSO",
    "acaoAtualizadaComSucesso": "ACAO_ATUALIZADO_COM_SUCESSO",
    "acaoRemovidaComSucesso": "ACAO_REMOVIDO_COM_SUCESSO"
}

export const msgColaborador = {
    "nenhumColaboradorEncontrado": "NENHUM_COLABORADOR_ENCONTRADO",
    "erroAoObterColaborador": "ERRO_AO_OBTER_COLABORADOR",
    "erroAoListarColaborador": "ERRO_AO_LISTAR_COLABORADOR",
    "colaboradorCampoNomeObrigatorio": "COLABORADOR_CAMPO_NOME_OBRIGATORIO",
    "erroAoInserirColaborador": "ERRO_AO_INSERIR_COLABORADOR",
    "erroAoAtualizarColaborador": "ERRO_AO_ATUALIZAR_COLABORADOR",
    "erroAoRemoverColaborador": "ERRO_AO_REMOVER_COLABORADOR",
    "erroAoObterFiliaisDoColaboradorAutenticado": "ERRO_AO_OBTER_FILIAIS_DO_COLABORADOR_AUTENTICADO",
    "colaboradorDeveSerUnico": "COLABORADOR_DEVE_SER_UNICO",
    "colaboradorCampoLoginObrigatorio": "COLABORADOR_CAMPO_LOGIN_OBRIGATORIO",
    "colaboradorCampoPasswordObrigatorio": "COLABORADOR_CAMPO_PASSWORD_OBRIGATORIO",
    "colaboradorCampoChaveGrupoDocumentoObrigatorio": "COLABORADOR_CAMPO_CHAVE_GRUPO_DOCUMENTO_OBRIGATORIO",
    "colaboradorCampoChaveGrupoContatoObrigatorio": "COLABORADOR_CAMPO_CHAVE_GRUPO_CONTATO_OBRIGATORIO",
    "colaboradorCampoRegexInvalido": "COLABORADOR_CAMPO_REGEX_INVALIDO",
    "colaboradorCampoQtMinCaractersInvalido": "COLABORADOR_CAMPO_QT_MIN_CARACTERS_INVALIDO",
    "colaboradorCampoQtMaxCaractersInvalido": "COLABORADOR_CAMPO_QT_MAX_CARACTERS_INVALIDO",
    "colaboradorAssociacaoTipoColaboradorNaoEncontrado": "COLABORADOR_ASSOCIADO_TIPO_COLABORADOR_NAO_ENCONTRADO"    
}

export const msgGeneric = {
    "nenhumRegistroEncontrado": "NENHUM_REGISTRO_ENCONTRADO",
    "erroAoObterRegistro"     : "ERRO_AO_OBTER_REGISTRO"
}

export const msgTipoDocumento = {
    "nenhumTipoDocumentoEncontrado": "NENHUM_TIPO_DOCUMENTO_ENCONTRADO",
    "erroAoObterTipoDocumento": "ERRO_AO_OBTER_TIPO_DOCUMENTO",
    "erroAoListarTipoDocumento": "ERRO_AO_LISTAR_TIPO_DOCUMENTO",
    "tipoDocumentoCampoNomeRangeObrigatorio": "TIPO_DOCUMENTO_CAMPO_NOME_RANGE_OBRIGATORIO",
    "erroAoInserirTipoDocumento": "ERRO_AO_INSERIR_TIPO_DOCUMENTO",
    "erroAoAtualizarTipoDocumento": "ERRO_AO_ATUALIZAR_TIPO_DOCUMENTO",
    "erroAoRemoverTipoDocumento": "ERRO_AO_REMOVER_TIPO_DOCUMENTO",
    "tipoDocumentoDeveSerUnico": "TIPO_DOCUMENTO_DEVE_SER_UNICO",
    "tipoDocumentoCampoTipoInvalido": "TIPO_DOCUMENTO_CAMPO_TIPO_INVALIDO",
    "tipoDocumentoCampoMaskInvalido": "TIPO_DOCUMENTO_CAMPO_MASK_INVALIDO",
    "tipoDocumentoCampoRegexInvalido": "TIPO_DOCUMENTO_CAMPO_REGEX_INVALIDO",
    "tipoDocumentoCampoQtMinCaractersInvalido": "TIPO_DOCUMENTO_CAMPO_QT_MIN_CARACTERS_INVALIDO",
    "tipoDocumentoCampoQtMaxCaractersInvalido": "TIPO_DOCUMENTO_CAMPO_QT_MAX_CARACTERS_INVALIDO"
}

export const msgTipoContato = {
    "nenhumTipoContatoEncontrado": "NENHUM_TIPO_CONTATO_ENCONTRADO",
    "erroAoObterTipoContato": "ERRO_AO_OBTER_TIPO_CONTATO",
    "erroAoListarTipoContato": "ERRO_AO_LISTAR_TIPO_CONTATO",
    "tipoContatoCampoNomeRangeObrigatorio": "TIPO_CONTATO_CAMPO_NOME_RANGE_OBRIGATORIO",
    "erroAoInserirTipoContato": "ERRO_AO_INSERIR_TIPO_CONTATO",
    "erroAoAtualizarTipoContato": "ERRO_AO_ATUALIZAR_TIPO_CONTATO",
    "erroAoRemoverTipoContato": "ERRO_AO_REMOVER_TIPO_CONTATO",
    "tipoContatoDeveSerUnico": "TIPO_CONTATO_DEVE_SER_UNICO",
    "tipoContatoCampoTipoInvalido": "TIPO_CONTATO_CAMPO_TIPO_INVALIDO",
    "tipoContatoCampoMaskInvalido": "TIPO_CONTATO_CAMPO_MASK_INVALIDO",
    "tipoContatoCampoRegexInvalido": "TIPO_CONTATO_CAMPO_REGEX_INVALIDO",
    "tipoContatoCampoQtMinCaractersInvalido": "TIPO_CONTATO_CAMPO_QT_MIN_CARACTERS_INVALIDO",
    "tipoContatoCampoQtMaxCaractersInvalido": "TIPO_CONTATO_CAMPO_QT_MAX_CARACTERS_INVALIDO"
}

export const msgContato = {
    "nenhumContatoEncontrado": "NENHUM_CONTATO_ENCONTRADO",
    "erroAoObterContato": "ERRO_AO_OBTER_CONTATO",
    "erroAoListarContato": "ERRO_AO_LISTAR_CONTATO",
    "contatoCampoValorRangeObrigatorio": "CONTATO_CAMPO_VALOR_RANGE_OBRIGATORIO",
    "erroAoInserirContato": "ERRO_AO_INSERIR_CONTATO",
    "erroAoAtualizarContato": "ERRO_AO_ATUALIZAR_CONTATO",
    "erroAoRemoverContato": "ERRO_AO_REMOVER_CONTATO",
    "contatoDeveSerUnico": "CONTATO_DEVE_SER_UNICO",
    "contatoCampoTipoInvalido": "CONTATO_CAMPO_TIPO_INVALIDO",
    "contatoCampoChaveGrupoInvalido": "CONTATO_CHAVE_GRUPO_INVALIDO",
    "contatoCampoRegexInvalido": "CONTATO_CAMPO_REGEX_INVALIDO",
    "contatoCampoQtMinCaractersInvalido": "CONTATO_CAMPO_QT_MIN_CARACTERS_INVALIDO",
    "contatoCampoQtMaxCaractersInvalido": "CONTATO_CAMPO_QT_MAX_CARACTERS_INVALIDO",
    "contatoAssociacaoTipoContatoNaoEncontrado": "CONTATO_ASSOCIADO_TIPO_CONTATO_NAO_ENCONTRADO"
}

export const msgDocumento = {
    "nenhumDocumentoEncontrado": "NENHUM_DOCUMENTO_ENCONTRADO",
    "erroAoObterDocumento": "ERRO_AO_OBTER_DOCUMENTO",
    "erroAoListarDocumento": "ERRO_AO_LISTAR_DOCUMENTO",
    "documentoCampoValorRangeObrigatorio": "DOCUMENTO_CAMPO_VALOR_RANGE_OBRIGATORIO",
    "erroAoInserirDocumento": "ERRO_AO_INSERIR_DOCUMENTO",
    "erroAoAtualizarDocumento": "ERRO_AO_ATUALIZAR_DOCUMENTO",
    "erroAoRemoverDocumento": "ERRO_AO_REMOVER_DOCUMENTO",
    "documentoDeveSerUnico": "DOCUMENTO_DEVE_SER_UNICO",
    "documentoCampoTipoInvalido": "DOCUMENTO_CAMPO_TIPO_INVALIDO",
    "documentoCampoChaveGrupoInvalido": "DOCUMENTO_CHAVE_GRUPO_INVALIDO",
    "documentoCampoRegexInvalido": "DOCUMENTO_CAMPO_REGEX_INVALIDO",
    "documentoCampoQtMinCaractersInvalido": "DOCUMENTO_CAMPO_QT_MIN_CARACTERS_INVALIDO",
    "documentoCampoQtMaxCaractersInvalido": "DOCUMENTO_CAMPO_QT_MAX_CARACTERS_INVALIDO",
    "documentoAssociacaoTipoDocumentoNaoEncontrado": "DOCUMENTO_ASSOCIADO_TIPO_DOCUMENTO_NAO_ENCONTRADO"
}

export const msgEscala = {
    "erroAoListarEscalasEmAno" : "ERRO_AO_LISTAR_ESCALAS_ANO",
    "erroFilialNaoLocalizadaParaOUsuario" : "ERRO_FILIAL_NAO_LOCALIZADA_PARA_O_USUARIO",
    "erroParametroPathAnoObrigatorio" : "ERRO_PARAMETRO_PATH_ANO_OBRIGATORIO",
    "erroParametroPathFilialObrigatorio" : "ERRO_PARAMETRO_PATH_FILIAL_OBRIGATORIO",
    "erroParametroBodyAnoObrigatorio" : "ERRO_PARAMETRO_BODY_ANO_OBRIGATORIO",
    "erroParametroBodyFilialObrigatorio" : "ERRO_PARAMETRO_BODY_FILIAL_OBRIGATORIO",
    "erroParametroBodyMesObrigatorio" : "ERRO_PARAMETRO_BODY_MES_OBRIGATORIO",
    "erroIniciarBaseEscalaParametrosComErro" : "ERRO_INICIAR_BASE_ESCALA_PARAMETROS_COM_ERRO",
    "erroIniciarBaseEscala" : "ERRO_INICIAR_BASE_ESCALA",
    "erroSoAdministradorPodeIniciarBaseEscala" : "ERRO_SO_ADMINISTRADOR_PODE_INICIAR_BASE_ESCALA",
    "erroIniciarBaseEscalaJaExecutado" : "ERRO_INICIAR_BASE_ESCALA_JA_EXECUTADO",
    //GERAR ESCALA
    "erroParametroPathIdMesAnoObrigatorio" : "ERRO_PARAMETRO_PATH_MES_ANO_OBRIGATORIO",
    "erroParametroBodyIdMesAnoObrigatorio" : "ERRO_PARAMETRO_BODY_MES_ANO_OBRIGATORIO",
    "erroGerarEscala" : "ERRO_GERAR_ESCALA",
    "erroSoAdministradorPodeGerarEscala" : "ERRO_SO_ADMINISTRADOR_PODE_GERAR_ESCALA",
    "erroGerarEscalaMesAnoNaoLocalizado" : "ERRO_GERAR_ESCALA_MES_ANO_NAO_LOCALIZADO",
    //GERAR ESCALA ANALISES -> //1 -> Aberto, 2 -> Fechado, 3 -> Liberado, 4 -> Cancelado e 5 -> Removido
    "erroGerarEscalaMesAnoEstaFechado" : "ERRO_GERAR_ESCALA_MES_ANO_ESTA_FECHADO",
    "erroGerarEscalaMesAnoEstaLiberado" : "ERRO_GERAR_ESCALA_MES_ANO_ESTA_LIBERADO",
    "erroGerarEscalaMesAnoEstaCancelado" : "ERRO_GERAR_ESCALA_MES_ANO_ESTA_CANCELADO",
    "erroGerarEscalaMesAnoEstaRemovido" : "ERRO_GERAR_ESCALA_MES_ANO_ESTA_REMOVIDO",
    //MUDAR TIPO TRABALHO NO DIA DO COLABORADOR
    "erroMudarTipoTrabalhoColaboradorDia" : "ERRO_MUDAR_TIPO_TRABALHO_COLABORADOR_DIA",
    "erroParametroBodyIdObrigatorio" : "ERRO_PARAMETRO_BODY_ID_OBRIGATORIO",
    "erroParametroBodyIdColaboradorObrigatorio" : "ERRO_PARAMETRO_BODY_ID_COLABORADOR_OBRIGATORIO",
    "erroParametroBodyTipoEscalaObrigatorio" : "ERRO_PARAMETRO_BODY_TIPO_ESCALA_OBRIGATORIO",
    "erroParametroBodyTipoEscalaNovaObrigatorio" : "ERRO_PARAMETRO_BODY_TIPO_ESCALA_NOVA_OBRIGATORIO",
    "erroParametroBodyIdDiaObrigatorio" : "ERRO_PARAMETRO_BODY_ID_DIA_OBRIGATORIO",
    //MUDAR STATUS ESCALA
    "erroMudarStatusEscala" : "ERRO_MUDAR_STATUS_ESCALA",
    "erroMudarStatusEscalaNaoExiste" : "ERRO_MUDAR_STATUS_ESCALA_NAO_EXITE",
    "erroSoAdministradorPodeMudarStatusEscala" : "ERRO_SO_ADMINISTRADOR_PODE_MUDAR_STATUS_ESCALA",
    "erroParametroBodyNovoStatusObrigatorio" : "ERRO_PARAMETRO_BODY_NOVO_STATUS_OBRIGATORIO",
}

export const msgFeriado = {
    "erroAoListarFeriados": "ERRO_AO_LISTAR_FERIADOS",
    "nenhumFeriadoEncontrado": "NENHUM_FERIADO_ENCONTRADO",
    "erroAoObterFeriado": "ERRO_AO_OBTER_FERIADO",
    "erroAoInserirFeriado": "ERRO_AO_INSERIR_FERIADO",
    "erroAoAtualizarFeriado": "ERRO_AO_ATUALIZAR_FERIADO",
    "erroAoRemoverFeriado": "ERRO_AO_REMOVER_FERIADO",
    "feriadoInseridoComSucesso": "FERIADO_INSERIDO_COM_SUCESSO",
    "feriadoAtualizadoComSucesso": "FERIADO_ATUALIZADO_COM_SUCESSO",
    "feriadoRemovidoComSucesso": "FERIADO_REMOVIDO_COM_SUCESSO",
    "feriadoDeveSerUnico": "FERIADO_DEVE_SER_UNICO",
    "feriadoCampoDiaAnoObrigatorio": "FERIADO_CAMPO_DIA_ANO_OBRIGATORIO",
    "feriadoCampoDiaUtilObrigatorio": "FERIADO_CAMPO_DIA_UTIL_OBRIGATORIO",
    "feriadoCampoFilialIdObrigatorio": "FERIADO_CAMPO_FILIAL_ID_OBRIGATORIO"    
}
