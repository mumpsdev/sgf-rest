import { Colaborador } from '../app/model/ColaboradorModel';
import { Sequelize } from "sequelize-typescript";
import { TipoDocumento } from '../app/model/TipoDocumentoModel';
import { Organizacao } from '../app/model/OrganizacaoModel';
import { Filial } from '../app/model/FilialModel';
import { Feriado } from '../app/model/FeriadosModel';
import { Perfil } from '../app/model/PerfilModel';
import { FilialPerfil } from '../app/model/FilialPerfilModel';
import { Acao } from '../app/model/AcaoModel';
import { PerfilAcao } from '../app/model/PerfilAcaoModel';
import { TipoPermissaoAcao } from '../app/model/TipoPermissaoAcao';
import { PermissaoAcao } from '../app/model/PermissaoAcaoModel';
import { Contato } from '../app/model/ContatoModel';
import { TipoContato } from '../app/model/TipoContatoModel';
import { Documento } from '../app/model/DocumentoModel';
import { MesAno } from '../app/model/escalas/MesAno';
import { Semana } from '../app/model/escalas/Semana';
import { Escala } from '../app/model/escalas/Escala';
import { Reserva } from '../app/model/escalas/Reserva';
import { Dia } from '../app/model/escalas/Dia';

export class FactoryDB{
    
    //Dados conexão Local.
    private host = "localhost";
    private port = 3306;
    private database = "dev_sgf_db";
    private user  = "dev_sgf_db";
    private password = "dev_sgf_db";
    private dialect = "mysql"
    private typeSync = "alter"; //Acredito que não funciona legal

    private sequelize: Sequelize;

    constructor(){
    }

    createConnection(onConnected: any, onError: any){
        let nodeEnv:string = process.env.NODE_ENV;
        console.log("Node_ENV: " + nodeEnv);
        if(!nodeEnv){
            console.log("---------------Ambiente Local");
        }else if(nodeEnv.indexOf("test") > -1){
            console.log("---------------Ambiente de Teste");
        }else if(nodeEnv.indexOf("development") > -1){
            console.log("---------------Ambiente desenvolvimento!");
        }else if(nodeEnv.indexOf("production") > -1){
            console.log("---------------Ambiente de produção!");
            this.host = "mysql785.umbler.com";
            this.port = 3306;
            this.password = "iZ*]c3GpC2#z";
        }
        this.sequelize =  new Sequelize({
            host: this.host,
            port: this.port,
            database: this.database,
            dialect: this.dialect,
            username: this.user,
            password: this.password,
            pool: {
                max: 25,
                min: 0,
                acquire: 30000,
                idle: 10000
            },
            logging: false
        });
        //Adicionando os modelos que serão espelhados como tabelas no banco de dados.
        this.sequelize.addModels([
            Colaborador,
            TipoDocumento,
            Organizacao,
            Filial,
            Feriado,
            Perfil,
            FilialPerfil,
            Acao,
            PerfilAcao,
            //PermissaoAcao,
            //TipoPermissaoAcao,
            Contato,
            TipoContato,
            Documento,
            MesAno,
            Semana,
            Dia,
            Escala,
            Reserva
        ]);
        
        this.sequelize.authenticate().then(onConnected).catch(onError);

    }

    updateTable(onUpdate: any, onError: any){
        if(this.sequelize){
            this.sequelize.sync().then(onUpdate).catch(onError);
        }
    }

    getSequelize() {
        return this.sequelize;
    }

    closeConnection(onClose: any){
        
    }
}