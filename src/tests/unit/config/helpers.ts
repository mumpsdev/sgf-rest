import * as mocha from 'mocha'
import * as Chai from 'chai'
import * as td from 'testdouble'
import * as supertes from 'supertest'
import App from '../../../app'


const app = App
const request = supertes
const expect = Chai.expect
const testDouble = td


export { app, expect, request, testDouble }