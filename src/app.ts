import { Sequelize } from 'sequelize-typescript';
import * as express from "express";
import * as http from "http";
import * as morgan from "morgan";
import * as bodyParser from "body-parser";
import * as expressValidator from 'express-validator';
import * as path from "path";
import SocketUtil from "./util/SocketUtil";
import * as cors from "cors";
import { FactoryDB } from "./util/FactoryDB";

// Módulos
import AuthTokenRest from "./app/controller/AuthTokenRest";
import OrganizacaoRest from "./app/controller/OrganizacaoRest";
import FilialRest from "./app/controller/FilialRest";
import PerfilRest from "./app/controller/PerfilRest";
import AcaoRest from "./app/controller/AcaoRest";
import ColaboradorRest from "./app/controller/ColaboradorRest";
import EscalaRest from './app/controller/EscalaRest';
import TipoDocumentoRest from './app/controller/TipoDocumentoRest';
import TipoContatoRest from './app/controller/TipoContatoRest';
import ContatoRest from './app/controller/ContatoRest';
import DocumentoRest from './app/controller/DocumentoRest';
import FeriadoRest from './app/controller/FeriadoRest';
import FilterRest from './app/controller/FilterRest';


class App {
    public exp: express.Application;
    public server: http.Server;
    private morgan: morgan.Morgan;
    private bodyParser; 
    private cors;
    private factoryDB: FactoryDB;
    
    constructor(){
        this.exp = express();//Criando servidor do Express
        this.server = SocketUtil.getServerSocketIo(this.exp);
        this.factoryDB = new FactoryDB();
        this.middlewares();
        this.initRoutes();
    }
    
    public createConnection(onConnected: Function, onError: Function){
       this.factoryDB.createConnection(onConnected, onError);
    }

    public updateTable(onUpate: Function, onError: Function){
        this.factoryDB.updateTable(onUpate, onError);
    }
    
    public closeConnection(onClose){
        // this.mondoDb.closeConnection(onClose);
    }

    private middlewares(){
        this.exp.use(morgan("dev"))
        this.exp.use(bodyParser.json())
        this.exp.use(bodyParser.urlencoded({extended: true}))
        this.exp.use(expressValidator())
        this.exp.use(express.static(path.join(__dirname, "public")))
        this.exp.use(cors())       
    }

    private initRoutes(){
        FilterRest.setRoutes(this.exp)//Filtro de chamadas que não são da api
        AuthTokenRest.setRoutes(this.exp)//Primeiro modulo, para fazer a validações dos acessos.
        OrganizacaoRest.setRoutes(this.exp)
        FilialRest.setRoutes(this.exp)
        PerfilRest.setRoutes(this.exp)
        AcaoRest.setRoutes(this.exp)
        ColaboradorRest.setRoutes(this.exp)
        EscalaRest.setRoutes(this.exp)
        TipoDocumentoRest.setRoutes(this.exp)
        TipoContatoRest.setRoutes(this.exp)
        ContatoRest.setRoutes(this.exp)
        DocumentoRest.setRoutes(this.exp)
        FeriadoRest.setRoutes(this.exp)
    }

    getSequelize() {
        return this.factoryDB.getSequelize();
    }
} 

export default new App();