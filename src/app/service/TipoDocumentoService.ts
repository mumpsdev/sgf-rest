import { TipoDocumento } from './../model/TipoDocumentoModel';
import { Data } from './../../util/Data';
import { FactoryDB } from './../../util/FactoryDB';
import { Sequelize } from 'sequelize-typescript';
import { msgTipoDocumento } from './../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import UtilService from '../../util/UtilService';

class TipoDocumentoService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await TipoDocumento.findById(id);

            if(result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgTipoDocumento.nenhumTipoDocumentoEncontrado,
                    'Nenhum Tipo de Documento encontrado',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoDocumento.erroAoObterTipoDocumento, error);
            res.status(500).json(data);
        }       
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            await data.executeQuery(req, res, TipoDocumento);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoDocumento.erroAoListarTipoDocumento, error);
            res.status(500).json(data);
        }  
    }

    // Cria um novo registro
    public async create(req, res) {
        let data = new Data();
        let tipoDocumento = req.body;
        
        // Campo nome deve conter no mín 2 e no máx 30 caracteres.
        req.assert('nome', msgTipoDocumento.tipoDocumentoCampoNomeRangeObrigatorio).len(2, 30);
        // Campo tipo não pode ser vazio nem ser diferente dos campos listados.
        req.assert('tipo', msgTipoDocumento.tipoDocumentoCampoTipoInvalido).notEmpty().isIn(['string', 'number', 'boolean', 'date']);
        // Campo mask não pode ser nulo.
        req.assert('mask', msgTipoDocumento.tipoDocumentoCampoMaskInvalido).notEmpty();
        // Campo regex não pode ser nulo.
        req.assert('regex', msgTipoDocumento.tipoDocumentoCampoRegexInvalido).notEmpty();
        // Campo qtMinCaracters não pode ser nulo e deve ser numérico.
        req.assert('qtMinCaracters', msgTipoDocumento.tipoDocumentoCampoQtMinCaractersInvalido).notEmpty().matches(/\d/);
        // Campo qtMaxCaracters não pode ser nulo e deve ser numérico.
        // Campo qtMaxCaracters deve ser maior que o campo qtMinCaracters.
        req.assert('qtMaxCaracters', msgTipoDocumento.tipoDocumentoCampoQtMaxCaractersInvalido).notEmpty().matches(/\d/)
            .isInt({ gt : req.body.qtMinCaracters});       

        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgTipoDocumento.erroAoInserirTipoDocumento, errors);                
                      
            res.status(400).send(data);
            return;
        }

        try {
            let tipoDocumentoEncontrado = await TipoDocumento.findOne({ where: { nome: tipoDocumento.nome }});

            if(tipoDocumentoEncontrado) {
                data.addMsg(tMsg.DANGER, msgTipoDocumento.tipoDocumentoDeveSerUnico); 
                res.status(500).send(data);
                return;
            } else {                
                let result = await TipoDocumento.create(tipoDocumento);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);                
                res.status(201).send(data);
            }
                        
        } catch (error) {           
            data.addMsgError(tMsg.DANGER, msgTipoDocumento.erroAoInserirTipoDocumento,
                error);
            
            res.status(500).send(data);
        } 
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let tipoDocumento = req.body;
        let id = req.params.id;

        let tipoDocumentoEncontrado;
        const Op = Sequelize.Op;

        let result;

        // Campo nome deve conter no mín 2 e no máx 30 caracteres.
        req.assert('nome', msgTipoDocumento.tipoDocumentoCampoNomeRangeObrigatorio).len(2, 30);
        // Campo tipo não pode ser vazio nem ser diferente dos campos listados.
        req.assert('tipo', msgTipoDocumento.tipoDocumentoCampoTipoInvalido).notEmpty().isIn(['string', 'number', 'boolean', 'date']);
        // Campo mask não pode ser nulo.
        req.assert('mask', msgTipoDocumento.tipoDocumentoCampoMaskInvalido).notEmpty();
        // Campo regex não pode ser nulo.
        req.assert('regex', msgTipoDocumento.tipoDocumentoCampoRegexInvalido).notEmpty();
        // Campo qtMinCaracters não pode ser nulo e deve ser numérico.
        req.assert('qtMinCaracters', msgTipoDocumento.tipoDocumentoCampoQtMinCaractersInvalido).notEmpty().matches(/\d/);
        // Campo qtMaxCaracters não pode ser nulo e deve ser numérico.
        // Campo qtMaxCaracters deve ser maior que o campo qtMinCaracters.
        req.assert('qtMaxCaracters', msgTipoDocumento.tipoDocumentoCampoQtMaxCaractersInvalido).notEmpty().matches(/\d/)
            .isInt({ gt : req.body.qtMinCaracters});  
        
        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgTipoDocumento.erroAoAtualizarTipoDocumento, errors);
                        
            res.status(400).send(data);
            return;
        }

        let tipoDocumentoUnico = await TipoDocumento.findOne({ where: { nome: tipoDocumento.nome }});
        
        if(tipoDocumentoUnico !== null && tipoDocumentoUnico.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgTipoDocumento.erroAoAtualizarTipoDocumento, 
                msgTipoDocumento.tipoDocumentoDeveSerUnico, "nome", tipoDocumento.nome); 
            res.status(500).send(data);
            return;
        }

        try {
            tipoDocumentoEncontrado = await TipoDocumento.findById(id);

            if(!tipoDocumentoEncontrado) {
                data.addMsg(tMsg.DANGER, msgTipoDocumento.nenhumTipoDocumentoEncontrado);
                res.status(500).send(data);
                return;

            } else { 
                tipoDocumento.dataAtualizacao = new Date();
                
                result = await TipoDocumento.update(tipoDocumento, {
                    where: {
                        id: id
                    }
                });

                if(result > 0) {
                    data.obj = tipoDocumento;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgTipoDocumento.erroAoAtualizarTipoDocumento);
                    res.status(500).send(data);
                }
                
                return;
            }
            
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoDocumento.erroAoAtualizarTipoDocumento, error);
            res.status(500).send(data);
        }        
    }

    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let tipoDocumentoEncontrado = await TipoDocumento.findById(id);           

            if(tipoDocumentoEncontrado) {
                result = await TipoDocumento.update({ativo: false}, {
                    where: {
                        id: id
                    }
                });               

                if(result > 0) {
                    res.status(200).send(result);

                } else {
                    data.addMsg(tMsg.DANGER, msgTipoDocumento.erroAoRemoverTipoDocumento);
                    res.status(500).send(tipoDocumentoEncontrado);
                }                

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgTipoDocumento.nenhumTipoDocumentoEncontrado,
                    'Nenhum tipo documento encontrado',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoDocumento.erroAoRemoverTipoDocumento, error);
            res.status(500).send(data);
        }
    }
}

export default new TipoDocumentoService();