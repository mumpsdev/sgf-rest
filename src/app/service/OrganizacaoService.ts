import { Organizacao } from './../model/organizacaoModel';
import { Data } from '../../util/Data';
import { msgOrganizacao } from '../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import { Sequelize } from 'sequelize-typescript';
import UtilService from '../../util/UtilService';

class OrganizacaoService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await Organizacao.findById(id);

            if(result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgOrganizacao.nenhumaOrganizacaoEncontrada,
                    'Nenhuma organizacao encontrada',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgOrganizacao.erroAoObterOrganizacao, error);
            res.status(500).json(data);
        }       
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            await data.executeQuery(req, res, Organizacao);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgOrganizacao.erroAoListarOrganizacoes, error);
            res.status(500).json(data);
        }  
    }

    // Cria um novo registro
    public async create(req, res) {
        let data = new Data();
        let organizacao = req.body;
        
        // Campo fantasia não pode ser vazio.
        req.assert('fantasia', msgOrganizacao.organizacaoCampoFantasiaObrigatorio).notEmpty();
        // Campo razão social não pode ser vazio.
        req.assert('razaoSocial', msgOrganizacao.organizacaoCampoRazaoSocialObrigatorio).notEmpty();
        // Campo chave grupo contato não pode ser nulo.
        req.assert('chaveGrupoContato', msgOrganizacao.organizacaoCampoChaveGrupoContatoObrigatorio).notEmpty();        
        // Campo chave grupo documento não pode ser nulo.
        req.assert('chaveGrupoDocumento', msgOrganizacao.organizacaoCampoChaveGrupoDocumentoObrigatorio).notEmpty();                

        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgOrganizacao.erroAoInserirOrganizacao, errors);                
                      
            res.status(400).send(data);
            return;
        }

        try {
            let organizacaoEncontrada = await Organizacao.findOne({ where: { fantasia: organizacao.fantasia }});

            if(organizacaoEncontrada) {
                data.addMsg(tMsg.DANGER, msgOrganizacao.organizacaoDeveSerUnica); 
                res.status(500).send(data);
                return;

            } else {                        
                let result = await Organizacao.create(organizacao);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);                
                res.status(201).send(data);
            }
                        
        } catch (error) {           
            data.addMsgError(tMsg.DANGER, msgOrganizacao.erroAoInserirOrganizacao,
                error);
            
            res.status(500).send(data);
        } 
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let organizacao = req.body;
        let id = req.params.id;

        let organizacaoEncontrado;
        const Op = Sequelize.Op;

        let result;

        // Campo fantasia não pode ser vazio.
        req.assert('fantasia', msgOrganizacao.organizacaoCampoFantasiaObrigatorio).notEmpty();
        // Campo razão social não pode ser vazio.
        req.assert('razaoSocial', msgOrganizacao.organizacaoCampoRazaoSocialObrigatorio).notEmpty();
        // Campo chave grupo contato não pode ser nulo.
        req.assert('chaveGrupoContato', msgOrganizacao.organizacaoCampoChaveGrupoContatoObrigatorio).notEmpty();        
        // Campo chave grupo documento não pode ser nulo.
        req.assert('chaveGrupoDocumento', msgOrganizacao.organizacaoCampoChaveGrupoDocumentoObrigatorio).notEmpty();                
        
        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgOrganizacao.erroAoAtualizarOrganizacao, errors);
                        
            res.status(400).send(data);
            return;
        }

        let organizacaoUnica = await Organizacao.findOne({ where: { fantasia: organizacao.fantasia }});        
        
        if(organizacaoUnica !== null && organizacaoUnica.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgOrganizacao.erroAoAtualizarOrganizacao, 
                msgOrganizacao.organizacaoDeveSerUnica, "fantasia", organizacao.fantasia); 
            res.status(500).send(data);
            return;
        }

        try {
            organizacaoEncontrado = await Organizacao.findById(id);

            if(!organizacaoEncontrado) {
                data.addMsg(tMsg.DANGER, msgOrganizacao.nenhumaOrganizacaoEncontrada);
                res.status(500).send(data);
                return;

            } else { 
                organizacao.dataAtualizacao = new Date();
                
                result = await Organizacao.update(organizacao, {
                    where: {
                        id: id
                    }
                });

                if(result > 0) {
                    data.obj = organizacao;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoAtualizarOrganizacao);
                    res.status(500).send(data);
                }
                
                return;
            }
            
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgOrganizacao.erroAoAtualizarOrganizacao, error);
            res.status(500).send(data);
        }        
    }
    
    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let organizacaoEncontrada = await Organizacao.findById(id);           

            if(organizacaoEncontrada) {
                result = await Organizacao.update({status: 4}, {
                    where: {
                        id: id
                    }
                });               

                if(result > 0) {
                    res.status(200).send(organizacaoEncontrada);

                } else {
                    data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoRemoverOrganizacao);
                    res.status(500).send(organizacaoEncontrada);
                }                

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgOrganizacao.nenhumaOrganizacaoEncontrada,
                    'Nenhuma organização encontrada',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgOrganizacao.erroAoRemoverOrganizacao, error);
            res.status(500).send(data);
        }
    }
}

export default new OrganizacaoService();