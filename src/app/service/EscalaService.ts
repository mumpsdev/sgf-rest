import { MesAno } from "../model/escalas/MesAno";
import { Colaborador } from "../model/ColaboradorModel";
import { FilialPerfil } from "../model/FilialPerfilModel";
import { Semana } from "../model/escalas/Semana";
import { Dia } from "../model/escalas/Dia";
import * as dateFns from 'date-fns'; //https://date-fns.org/
import { Data } from "../../util/Data";
import { tMsg } from "../../util/Values";
import { msgEscala } from "../../util/MessageTranslate";
import { Escala } from "../model/escalas/Escala";
import { Reserva } from "../model/escalas/Reserva";
import * as later from 'later'; //https://bunkat.github.io/later
import * as nodeCalendar from 'node-calendar';//https://github.com/ArminTamzarian/node-calendar
import * as shuffle from 'shuffle-array';//https://github.com/pazguille/shuffle-array
import * as lodash from 'lodash'; //https://lodash.com/
import { config } from "bluebird";

class EscalaService {

    constructor(){
    }

    public async AnosComEscalasResumo(req, res) {
        let data = new Data()
        let result: {[k: string]: any} = {}
        try {

            let filial = req.params.filial
            let userLogged = req["login"]

            // parametro filial não pode ser vazio.
            req.assert('filial', msgEscala.erroParametroPathFilialObrigatorio).notEmpty();

            let errors = req.validationErrors();
            
            if (errors) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroAoListarEscalasEmAno, errors)               
                res.status(400).send(data)
                return
            }

            let colaborador = await Colaborador.findOne( 
                { where: { "login" : userLogged["login"],  "status" : 2 } ,
                attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao" ] },
                include : [ { model : FilialPerfil, as : 'filialPerfil', where : { filialId : filial } } ]  
            })

            if (!colaborador) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                    res.status(404).json(data)
                    return
            }

            let anos = await MesAno.findAll({
                where : { filialId : filial }, group : "ano" , order : ["ano"] , 
                attributes: { exclude: [ "dataCadastro", "dataAtualizacao", "label", "mes" ] }
            })

            if (!anos) {
                data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                res.status(404).json(data)
                return
            }   

            result.anos = anos

            data.obj = result
            res.json(data)

        } catch (erro) {
            data.addMsg(tMsg.DANGER, msgEscala.erroAoListarEscalasEmAno)
            res.sendStatus(500).json(data)
        }
    }

    public async escalasDoAnoResumo(req, res) {
        let data = new Data()
        let result: {[k: string]: any} = {}

        try {

            let filial = req.params.filial
            let anoRequest = req.params.ano
            let userLogged = req["login"]

             // parametro filial não pode ser vazio.
            req.assert('filial', msgEscala.erroParametroPathFilialObrigatorio).notEmpty();
            // parametro ano contato não pode ser vazio.
            req.assert('ano', msgEscala.erroParametroPathAnoObrigatorio).notEmpty();

            let errors = req.validationErrors();
            
            if (errors) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroAoListarEscalasEmAno, errors)               
                res.status(400).send(data)
                return
            }

            let colaborador = await Colaborador.findOne( 
                { where: { "login" : userLogged["login"],  "status" : 2 } ,
                attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao" ] },
                include : [ { model : FilialPerfil, as : 'filialPerfil', where : { filialId : filial } } ]  
            })

            if (!colaborador) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                    res.status(404).json(data)
                    return
            }

            let mes = await MesAno.findAll({
                where : { ano : anoRequest, filialId : filial }, order : [ "mes" ],
                attributes: { exclude: [ "dataCadastro", "dataAtualizacao" ] }
            })

            if (!mes) {
                data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                res.status(404).json(data)
                return
            }   

            result.mes = mes

            data.obj = result
            res.json(data)
        } catch (erro) {
            data.addMsg(tMsg.DANGER, msgEscala.erroAoListarEscalasEmAno)
            res.sendStatus(500).json(data)
        }
    }

    /**
     * Metodo que inicia base para escalas, criando entidas [MesAno -> [Semana -> [Dia] ] ]
     * @param req Request da requisição
     * @param res Response da Requisição
     */
    public async iniciarBaseEscala(req, res) {
        let data = new Data()
        let result: {[k: string]: any} = {}
        //Pega informações do body
        let conteudoBody = req.body
        //pega usuario logado
        let usuarioLogado = req["login"]

        try {

            // Campo filial não pode ser vazio.
            req.assert('filial', msgEscala.erroParametroBodyFilialObrigatorio).notEmpty().isNumeric().isInt({ min: 1})
            // Campo mes não pode ser vazio.
            req.assert('mes', msgEscala.erroParametroBodyMesObrigatorio).notEmpty().isNumeric().isInt({ min: 1, max: 12})
            // Campo ano não pode ser nulo.
            req.assert('ano', msgEscala.erroParametroBodyAnoObrigatorio).notEmpty().isNumeric().isInt({ min: 1900, max: 2050})
            
            let errors = req.validationErrors()
            
            //Validando parametros enviados
            if (errors) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroIniciarBaseEscala, errors)                
                res.status(400).send(data)
                return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            //Busca com base no colaborador logado e filial informada na requisição
            let colaborador = await Colaborador.findOne( 
                { where: { login : usuarioLogado["login"], status : 2, isAdmin : true } ,
                attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao" ] },
                include : [ { model : FilialPerfil, as : 'filialPerfil', where : { filialId : conteudoBody.filial } } ]  
            })

            if (!colaborador) {
                if (!colaborador.isAdmin) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroSoAdministradorPodeIniciarBaseEscala)
                    res.status(401).json(data)
                } else {
                    data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                    res.status(400).json(data)
                }
                return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            //Buscando MesAno para saber se já existe dados criado por outra requisição antes
            let mesAnoValidacao = await MesAno.findOne({
                where : { ano : conteudoBody.ano, filialId : conteudoBody.filial, mes : conteudoBody.mes }
            })

            if (mesAnoValidacao) {
                data.addMsg(tMsg.DANGER, msgEscala.erroIniciarBaseEscalaJaExecutado)
                res.status(500).json(data)
                return
            } else {
                //Aqui devera começar a criar a base da escala
                let labelMesAno = conteudoBody.mes + '/' + conteudoBody.ano
                //Criando o mes/ano
                let mesAnoNovo = await MesAno.create({ 
                    'label' : labelMesAno, 
                    'mes' : conteudoBody.mes,
                    'ano' : conteudoBody.ano,
                    'status' : 1,
                    'filialId' : conteudoBody.filial 
                })
                
                //Verifica se o registro foi criado corretamente
                if (mesAnoNovo) {

                    //Data base do mês
                    let dataBaseMesAno = new Date(conteudoBody.ano, (conteudoBody.mes - 1), 1)

                    //Semanas do mês e seus dias em formato
                    let totalDeSemanas = new nodeCalendar.Calendar(0).monthdatescalendar(dataBaseMesAno.getFullYear(), ( dataBaseMesAno.getMonth() + 1 ))

                    //intera no objeto para criar as semanas. obs.: dentro do laço estou criando tambem os dias dos meses.
                    for (let i = 0; i < totalDeSemanas.length; i++) {
                        //numero da semana
                        let numeroSemana = i + 1
                        for (let j = 0; j < totalDeSemanas[i].length; j++) {

                            //primeiro dia da semana no laço
                            let dtEach = new Date(totalDeSemanas[i][j])

                            //console.log('Semana -> ' + numeroSemana + ' - Numero Semana -> ' + later.weekOfYear.val(dtEach))

                            let numSemanaAnoValue = later.weekOfYear.val(dtEach)
                            let semanaNovaValue = 'Semana ' + numeroSemana

                            if (conteudoBody.mes == 1 && numSemanaAnoValue >= 52) {
                                semanaNovaValue = 'Semana ' + numeroSemana + ' - ajuste'
                            }

                            //cria a semana no banco de dados
                            let semanaNova = await Semana.create({
                                'label' : semanaNovaValue,
                                'numSemanaAno' : numSemanaAnoValue,
                                'MesAnoId' : mesAnoNovo.id
                            })

                            //Verifica se o registro foi criado corretamente
                            if (semanaNova) {

                                //Pega o array de dias da semana na interação atual
                                let diasDaSemana = totalDeSemanas[i]
                                //intera nos dias da semanas
                                for (let k = 0; k < diasDaSemana.length; k++) {
                                    //Primeiro dia da semana
                                    let dtDia = diasDaSemana[k]
                                    //Numero do dia na semana
                                    let numDiaSemana = k
                                    //Numero do dia no ano
                                    let numDiaAno = dateFns.getDayOfYear(dtDia)

                                    if (dtDia.getMonth() == (conteudoBody.mes - 1) ) {
                                        //cria o registro na base de dados Dia
                                        let diaNovo = await Dia.create({
                                            'numDiaSemana' : numDiaSemana,
                                            'numDiaAno' : numDiaAno,
                                            'semanaId' : semanaNova.id
                                        })

                                        //Verifica se o registro foi criado corretamente
                                       if (diaNovo) {
                                           //Criou o registro corretamente.
                                       } else {
                                           data.addMsg(tMsg.DANGER, msgEscala.erroIniciarBaseEscalaJaExecutado)
                                           res.status(500).json(data)
                                           return
                                       }

                                    } else {
                                        //Dia não entra no mês pela regra do ISO 8901
                                    }

                                }

                            } else {
                                data.addMsg(tMsg.DANGER, msgEscala.erroIniciarBaseEscalaJaExecutado)
                                res.status(500).json(data)
                                return
                            }

                            break
                        }
                    }

                } else {
                    data.addMsg(tMsg.DANGER, msgEscala.erroIniciarBaseEscalaJaExecutado)
                    res.status(500).json(data)
                    return
                }


                //Teoricamente se chegou até aqui esta tudo certinho, sendo assim retorna o mês/ano criado 
                result.mesAno = mesAnoNovo
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            data.obj = result
            res.status(201).json(data)
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgEscala.erroIniciarBaseEscala, error)
            res.status(500).send(data)
        }
    }

    public async gerarEscalaMes(req, res) {

        let data = new Data()
        let result: {[k: string]: any} = {}
        let funcionarios: {[k: string]: any} = {}
        let colaboradorIds: Array<any> = []
        let colaboradores: {[k: string]: any} = {}

        let conteudoBody = req.body
        //pega usuario logado
        let usuarioLogado = req["login"]

        try {

            // Campo id não pode ser vazio.
            req.assert('id', msgEscala.erroParametroBodyIdMesAnoObrigatorio).notEmpty().isNumeric().isInt( { min: 1 } )

            let errors = req.validationErrors()

            //Validando parametros enviados
            if (errors) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroGerarEscala, errors)                
                res.status(400).send(data)
                return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            let mesAno = await MesAno.findOne( { where: { id : conteudoBody.id } } )

            if (!mesAno) {
                data.addMsgError(tMsg.DANGER, msgEscala.erroGerarEscalaMesAnoNaoLocalizado, errors)                
                res.status(400).send(data)
                return
            } 
            //1 -> Aberto, 2 -> Liberado, 3 -> Fechado, 4 -> Cancelado e 5 -> Removido
            if (mesAno.status == 2) {
                data.addMsgError(tMsg.DANGER, msgEscala.erroGerarEscalaMesAnoEstaLiberado, errors)                
                res.status(400).send(data)
                return
            } else if (mesAno.status == 3) {
                data.addMsgError(tMsg.DANGER, msgEscala.erroGerarEscalaMesAnoEstaFechado, errors)                
                res.status(400).send(data)
                return
            } else if (mesAno.status == 4) {
                data.addMsgError(tMsg.DANGER, msgEscala.erroGerarEscalaMesAnoEstaCancelado, errors)                
                res.status(400).send(data)
                return
            } else if (mesAno.status == 5) {
                data.addMsgError(tMsg.DANGER, msgEscala.erroGerarEscalaMesAnoEstaRemovido, errors)                
                res.status(400).send(data)
                return
            }

            //Busca com base no colaborador logado e filial informada na requisição
            let colaborador = await Colaborador.findOne( 
                { where: { login : usuarioLogado["login"], status : 2, isAdmin : true } ,
                attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao" ] },
                include : [ { model : FilialPerfil, as : 'filialPerfil', where : { filialId : mesAno.filialId } } ]  
            })

            if (!colaborador) {
                if (!colaborador.isAdmin) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroSoAdministradorPodeGerarEscala)
                    res.status(401).json(data)
                } else {
                    data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                    res.status(400).json(data)
                }
                return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            funcionarios.filialPerfis = await FilialPerfil.findAll(
                { where : { filialId : mesAno["filialId"] },
                include : [ { model : Colaborador, as : 'colaborador', where : { status : 2 , diasFolga : { $not: null }} }] 
                })

            // Percorrendo todos os funcionarios da filial selecionada para escala
            funcionarios.filialPerfis.forEach(element => {
                    colaboradorIds.push(element["id"])
                })
            
            //Nota Bsucar colaborador usando a profundidade de relacionamento evitando as conulta anteriores   
            colaboradores = await Colaborador.findAll( { where: { 
                "id" : { in : colaboradorIds } ,
                "status" :  2 } } )

            let semanas = await Semana.findAll( { where : { MesAnoId : mesAno["id"] }  , order : [ 'id' ],
                    include : [ { model : Dia, as : 'dia'  } ]
                } )

            //pegando todos os domingos do mes para realizar fator de folga para domingos
            let domingosNoMes = await MesAno.findAndCount({
                where : { id : mesAno["id"] },
                attributes: [], 
                include : [ { model : Semana, as : 'semana', attributes: [],
                        include : [ { model : Dia, as : 'dia', where : { numDiaSemana : 6 } , attributes: [] } ] } ]
            })

            
            //Calcula fator de folga para domgos usando o Math.round para arredondar para cima, uma opção é usar o Math.trunc para aredondar para baixo
            let fatorQtColaboradoresDomingoParaFolgar = Math.round(colaboradores.length / domingosNoMes.count)
            
            //Capturando objeto em list para o algoritimo saber quem ainda precisa folgar no domingo
            let precisaFolgar: Array<any> = []
            
            colaboradores.forEach(elem => {
                precisaFolgar.push(elem)
            })
            //////////////////////////////////////////////////////////////////////////////////////////

            semanas.forEach( element => {
                //dados da semana
                //console.log('Semana -> ' + element["label"] + ' - ' + element['id'])
                let dias = element['dia']
                let colaboradoresContext = colaboradores

                dias.forEach(dia => {

                    //Verifica se um domingo para fazer a escala seguindo orientações especificas
                    if (dia['numDiaSemana'] == 6) {
                        //gera embaralhamento na lista de colaboradores que precisam folga, evitando folgas em sequencia ou ao menos diminuindo a sequencia
                        shuffle(precisaFolgar)
                        //Elege e retira os colaboradores que irão folgar neste domingo do objeto @precisaFolgar, evitando que o mesmo folge novamente
                        let folgasDesteDomingo = precisaFolgar.splice(0, fatorQtColaboradoresDomingoParaFolgar)

                        //Cadastra no banco as folgas para os elegidos deste domingo
                        folgasDesteDomingo.forEach(folgados => {
                            
                            let escalaSave : {[k: string]: any} = {}
                
                            escalaSave.diaId = dia['id']
                            escalaSave.colaboradorId = folgados['id']
                            escalaSave.tipoEscala = 2
                
                            let result = Escala.create(escalaSave)
                
                        })

                        //Cadastra no banco os colaboradores deste domingo
                        colaboradores.forEach(deuRuimDomingao => {

                            //valida baseado na lista de colaboradores se o mesmo esta folgando
                            let filtroDeuRuim = lodash.find(folgasDesteDomingo, { id : deuRuimDomingao.id }) == undefined ? true : false
                            // se @filtroDeuRuim for true então o colaborador não foi encontrdo na lista de @folgasDesteDomingo sendo assim ele vai trabalhar 
                            if (filtroDeuRuim) {
                                    let escalaSave : {[k: string]: any} = {}
                        
                                    escalaSave.diaId = dia['id']
                                    escalaSave.colaboradorId = deuRuimDomingao['id']
                                    escalaSave.tipoEscala = 1
                        
                                    let result = Escala.create(escalaSave)
                            }
                
                        })

                    } else {
                        //Se entrou dentro do else é por que é um dia da semana diferente do domingo
                        let folgadores = colaboradores.filter( (element, index, array) => {
                            return (parseInt(element['diasFolga']) == dia['numDiaSemana'])
                        })
                        
                        folgadores.forEach(folgados => {
                            
                            let escalaSave : {[k: string]: any} = {}
                
                            escalaSave.diaId = dia['id']
                            escalaSave.colaboradorId = folgados['id']
                            escalaSave.tipoEscala = 2
                
                            let result = Escala.create(escalaSave)
                
                        })
                
                        let trabalhadores = colaboradores.filter( (element, index, array) => {
                            return (parseInt(element['diasFolga']) != dia['numDiaSemana'])
                        })
                        
                        trabalhadores.forEach(deuRuim => {
                            
                            let escalaSave : {[k: string]: any} = {}
                
                            escalaSave.diaId = dia['id']
                            escalaSave.colaboradorId = deuRuim['id']
                            escalaSave.tipoEscala = 1
                
                            let result = Escala.create(escalaSave)
                
                        })

                    }

                })

            })

            let liberandoMeAno = await MesAno.update( { status : 2 }, {
                where: {
                    id : mesAno.id
                }
            })

            result.status = liberandoMeAno
            result.mesAno = mesAno.reload()
            data.obj = result
            res.status(201).send(data)

        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgEscala.erroGerarEscala, error)
            res.status(500).send(data)
        }

    }

    public async escalaDoMesCompleto(req, res) {
        let data = new Data()
        let result: {[k: string]: any} = {}

        try {

            let filial = req.params.filial
            let anoRequest = req.params.ano
            let mesRequest = req.params.mes
            let mes: {[k: string]: any} = {}
            let userLogged = req["login"]

             // parametro filial não pode ser vazio.
            req.assert('filial', msgEscala.erroParametroPathFilialObrigatorio).notEmpty().isNumeric().isInt( { min: 1 } )
            // parametro ano contato não pode ser vazio.
            req.assert('ano', msgEscala.erroParametroPathAnoObrigatorio).notEmpty().isNumeric().isInt( { min: 1900, max: 2050 } )
            // parametro mes contato não pode ser vazio.
            req.assert('mes', msgEscala.erroParametroPathAnoObrigatorio).notEmpty().isNumeric().isInt( { min: 1, max: 12 } )

            let errors = req.validationErrors();
            
            if (errors) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroAoListarEscalasEmAno, errors)               
                res.status(400).send(data)
                return
            }

            let colaborador = await Colaborador.findOne( 
                { where: { "login" : userLogged["login"],  "status" : 2 } ,
                attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao" ] },
                include : [ { model : FilialPerfil, as : 'filialPerfil', where : { filialId : filial } } ]  
            })

            if (!colaborador) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                    res.status(404).json(data)
                    return
            }

            if (!colaborador.isAdmin) {
                mes = await MesAno.findAll({
                    where : { ano : anoRequest, filialId : filial, mes : mesRequest },
                    attributes: { exclude: [ "dataCadastro", "dataAtualizacao" ] }, 
                    include : [ { model : Semana, as : 'semana', 
                            include : [ { model : Dia, as : 'dia',
                                include : [ { model : Escala, as : 'escala', include : [ { model : Colaborador, as : "colaborador", where : { id : colaborador.id }, attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao", "chaveGrupoDocumento", "chaveGrupoContato" ] } }]} ] 
                            }, { model : Reserva , as : "reserva" , include : [ { model : Colaborador, as : "colaborador", where : { id : colaborador.id }, attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao", "chaveGrupoDocumento", "chaveGrupoContato" ] }  }]} ] } ]
                })
            } else {
                mes = await MesAno.findAll({
                    where : { ano : anoRequest, filialId : filial, mes : mesRequest },
                    attributes: { exclude: [ "dataCadastro", "dataAtualizacao" ] }, 
                    include : [ { model : Semana, as : 'semana', 
                            include : [ { model : Dia, as : 'dia',
                                include : [ { model : Escala, as : 'escala', include : [ { model : Colaborador, as : "colaborador", attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao", "chaveGrupoDocumento", "chaveGrupoContato" ] } }]} ] 
                            }, { model : Reserva , as : "reserva" , include : [ { model : Colaborador, as : "colaborador", attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao", "chaveGrupoDocumento", "chaveGrupoContato" ] }  }]} ] } ]
                })
            }

            if (!mes) {
                data.addMsg(tMsg.DANGER, msgEscala.erroFilialNaoLocalizadaParaOUsuario)
                res.status(404).json(data)
                return
            }   

            result.mes = mes

            data.obj = result
            res.status(200).json(data)
        } catch (erro) {
            data.addMsg(tMsg.DANGER, msgEscala.erroAoListarEscalasEmAno)
            res.status(500).json(data)
        }
    }

    public async mudarTipoTrabalhoColaborador(req, res) {
        let data = new Data()
        let result: {[k: string]: any} = {}
        //Pega informações do body
        let conteudoBody = req.body
        //pega usuario logado
        let usuarioLogado = req["login"]

        try {

            // Campo id da escala não pode ser vazio.
            req.assert('id', msgEscala.erroParametroBodyIdObrigatorio).notEmpty().isNumeric().isInt({ min: 1 })
            // Campo id do colaborador não pode ser vazio.
            req.assert('colaboradorId', msgEscala.erroParametroBodyIdColaboradorObrigatorio).notEmpty().isNumeric().isInt({ min: 1 })
            // Campo id do colaborador não pode ser vazio.
            req.assert('diaId', msgEscala.erroParametroBodyIdDiaObrigatorio).notEmpty().isNumeric().isInt({ min: 1 })
            // Campo tipo de escala não pode ser vazio.
            req.assert('tipoEscala', msgEscala.erroParametroBodyTipoEscalaObrigatorio).notEmpty().isNumeric().isInt({ min: 1, max : 2 })
            // Campo tipo de escala não pode ser vazio.
            req.assert('tipoEscalaNova', msgEscala.erroParametroBodyTipoEscalaNovaObrigatorio).notEmpty().isNumeric().isInt({ min: 1, max : 2 })

            let errors = req.validationErrors()
            
            //Validando parametros enviados
            if (errors) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroMudarTipoTrabalhoColaboradorDia, errors)                
                res.status(400).send(data)
                return
            }

            let escalaRequest = await Escala.findOne({
                where: {
                    id: conteudoBody.id,
                    colaboradorId: conteudoBody.colaboradorId,
                    diaId: conteudoBody.diaId,
                    tipoEscala: conteudoBody.tipoEscala
                },
                include: [ {
                    model: Dia, as: 'dia', include: [ {
                        model: Semana, as: 'semana',
                        include: [ { model: MesAno, as: 'mesAno', where: { status: 2 } }]
                    } ]
                } ]
            })

            if (!escalaRequest.dia.semana) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroMudarTipoTrabalhoColaboradorDia, errors)                
                res.status(400).send(data)
                return
            }

            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 
            let mudancaDeEscala = await Escala.update({ tipoEscala: conteudoBody.tipoEscalaNova }, {
                where: {
                    id: conteudoBody.id,
                    colaboradorId: conteudoBody.colaboradorId,
                    diaId: conteudoBody.diaId,
                    tipoEscala: conteudoBody.tipoEscala
                }
            })

            if (mudancaDeEscala[0] == 0) {
                data.addMsgError(tMsg.DANGER, msgEscala.erroMudarTipoTrabalhoColaboradorDia, errors)                
                res.status(204).send(data)
                return
            } else {
                result.status = mudancaDeEscala
            }

            result.escala = await escalaRequest.reload()
            data.obj = result
            res.status(200).json(data)
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgEscala.erroMudarTipoTrabalhoColaboradorDia, error)
            res.status(500).send(data)
        }
    }

    public async mudaStatusEscala (req, res) {

        let data = new Data()
        let result: {[k: string]: any} = {}
        //Pega informações do body
        let conteudoBody = req.body
        //pega usuario logado
        let usuarioLogado = req["login"]

        try {

            // Campo id da escala não pode ser vazio.
            req.assert('id', msgEscala.erroParametroBodyIdObrigatorio).notEmpty().isNumeric().isInt({ min: 1 })
            // Campo novoStatus não pode ser vazio.
            req.assert('novoStatus', msgEscala.erroParametroBodyNovoStatusObrigatorio).notEmpty().isNumeric().isInt( { min : 1, max : 5 } )

            let errors = req.validationErrors()
            
            //Validando parametros enviados
            if (errors) { 
                data.addMsgError(tMsg.DANGER, msgEscala.erroMudarStatusEscala, errors)                
                res.status(400).send(data)
                return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _

            //Busca escala que esta na requeste para mudança de status
            let escalaMudarStatus = await MesAno.findOne({
                where: {
                    id : conteudoBody.id,
                }
            })

            if (!escalaMudarStatus) {
                data.addMsg(tMsg.DANGER, msgEscala.erroMudarStatusEscalaNaoExiste)
                res.status(204).json(data)
                return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            //Busca com base no colaborador logado e filial informada na requisição
            let colaborador = await Colaborador.findOne( 
                { where: { login : usuarioLogado["login"], status : 2, isAdmin : true } ,
                attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao" ] }  
            })

            if (!colaborador) {
                if (!colaborador.isAdmin) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroSoAdministradorPodeMudarStatusEscala)
                    res.status(401).json(data)
                } else {
                    data.addMsg(tMsg.DANGER, msgEscala.erroMudarStatusEscala)
                    res.status(400).json(data)
                }
                return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            //Verificando o status para validar ou não a operação
            if ( escalaMudarStatus.status == 1 ) {
                if (conteudoBody.novoStatus != 2) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroMudarStatusEscalaNaoExiste)
                    res.status(204).json(data)
                    return
                }
            } else if (escalaMudarStatus.status == 2) {
                if (conteudoBody.novoStatus != 3) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroMudarStatusEscalaNaoExiste)
                    res.status(204).json(data)
                    return
                }
            } else if (escalaMudarStatus.status == 3) {
                if (conteudoBody.novoStatus != 2 && conteudoBody.novoStatus != 4 && conteudoBody.novoStatus != 5) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroMudarStatusEscalaNaoExiste)
                    res.status(204).json(data)
                    return
                }
            } else if (escalaMudarStatus.status == 4) {
                if (conteudoBody.novoStatus != 5) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroMudarStatusEscalaNaoExiste)
                    res.status(204).json(data)
                    return
                }
            } else if (escalaMudarStatus.status == 5) {
                    data.addMsg(tMsg.DANGER, msgEscala.erroMudarStatusEscalaNaoExiste)
                    res.status(204).json(data)
                    return
            }
            // - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ 

            //Realiza o update no registro seguindo informações
            let escalaMudarStatusAtualizacao = await MesAno.update( { status : conteudoBody.novoStatus }, {
                where: {
                    id : conteudoBody.id,
                }
            })

            //verifica se o registro foi atualizado ou não 
            if (escalaMudarStatusAtualizacao[0] == 0) {
                data.addMsgError(tMsg.DANGER, msgEscala.erroMudarStatusEscala, errors)                
                res.status(204).send(data)
                return
            } else {
                result.status = escalaMudarStatusAtualizacao
            }

            result.mesAno = await escalaMudarStatus.reload()
            data.obj = result
            res.status(200).json(data)
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgEscala.erroMudarStatusEscala, error)
            res.status(500).send(data)
        }
    }

    // public async gerarFolgasDomingos (req, res) {
    //     let data = new Data()
    //     let result: {[k: string]: any} = {}
    //     let funcionarios: {[k: string]: any} = {}
    //     let colaboradorIds: Array<any> = []
    //     let colaboradores: {[k: string]: any} = {}

    //     funcionarios.filialPerfis = await FilialPerfil.findAll(
    //         { where : { filialId : 1 },
    //         include : [ { model : Colaborador, as : 'colaborador', where : { status : 2 , diasFolga : { $not: null }, isAdmin : 0 } }] 
    //         })

    //     // Percorrendo todos os funcionarios da filial selecionada para escala
    //     funcionarios.filialPerfis.forEach(element => {
    //             colaboradorIds.push(element["id"])
    //         })
        
    //     //Nota Bsucar colaborador usando a profundidade de relacionamento evitando as conulta anteriores   
    //     colaboradores = await Colaborador.findAll( { where: { 
    //         "id" : { in : colaboradorIds } ,
    //         "status" :  2 } } )
        
    //      result.domingosNoMes = await MesAno.findAndCount({
    //         where : { ano : 2000, filialId : 1, mes : 2 },
    //         attributes: [], 
    //         include : [ { model : Semana, as : 'semana', attributes: [],
    //                 include : [ { model : Dia, as : 'dia', where : { numDiaSemana : 6 } , attributes: [] } ] } ]
    //     })

    //     console.log(colaboradores.length)
    //     //Arredonda para baixo
    //     console.log("Arredonda para baixo -> " + Math.trunc(colaboradores.length / result.domingosNoMes.count))
    //     //Arredonda para cima
    //     console.log("Arredonda para cima -> " + Math.round(colaboradores.length / result.domingosNoMes.count))

    //     let fatorQtColaboradoresDomingoParaFolgar = Math.round(colaboradores.length / result.domingosNoMes.count)
    //     let precisaFolgar: Array<any> = []

    //     colaboradores.forEach(ids => {
    //         precisaFolgar.push(ids)
    //     })

    //     for (let i = 1; i <= result.domingosNoMes.count; i++) {
    //         shuffle(precisaFolgar)
    //         let folgasDesteDomingo = precisaFolgar.splice(0, fatorQtColaboradoresDomingoParaFolgar)
    //         console.log("Total de trabalhadores -> " + colaboradores.length)
    //         console.log(" Folgando neste domingo " + i + " -> " + folgasDesteDomingo)

    //         let trabalhadoresDesteDomingo: Array<any> = []
    //         for (let h = 0; h < colaboradores.length; h++) {
    //             let filtroFolgando = false

    //             // for (let t = 0; t < folgasDesteDomingo.length; t++) {
    //             //     if (colaboradorIds[h] == folgasDesteDomingo[t]) {
    //             //         filtroFolgando = true
    //             //         console.log("O trabalhador esta folgando " + colaboradorIds[h])
    //             //         break
    //             //     }
    //             // }
    //             let filtroFind = lodash.find(folgasDesteDomingo, { id : colaboradores[h].id })

    //             console.log("Trabalhador  " + colaboradores[h].id + " Esta folgando ? - >" + (filtroFind == undefined ? false : true))

    //             // if (!lodash.find(folgasDesteDomingo, { id : colaboradorIds[h].id })) {
    //             //     console.log("O trabalhador esta trabalhando " + colaboradorIds[h])
    //             //     trabalhadoresDesteDomingo.push(colaboradorIds[h])
    //             // }

    //             // if (!filtroFolgando) {
    //             //     console.log("O trabalhador esta trabalhando " + colaboradorIds[h])
    //             //     trabalhadoresDesteDomingo.push(colaboradorIds[h])
    //             // }

    //         }
    //         //console.log(" Trabalhando neste domingo " + trabalhadoresDesteDomingo.length + " -> " + trabalhadoresDesteDomingo)
    //         //console.log(" - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ ")
    //     }

    //     data.obj = result
    //     res.status(200).json(data)
    // }

        // private aplicarEscalas(dia, colaboradores) {
    //     let folgadores = colaboradores.filter( (element, index, array) => {
    //         return (parseInt(element['diasFolga']) == dia['numDiaSemana'])
    //     })
        
    //     folgadores.forEach(folgados => {
            
    //         let escalaSave : {[k: string]: any} = {}

    //         escalaSave.diaId = dia['id']
    //         escalaSave.colaboradorId = folgados['id']
    //         escalaSave.tipoEscala = 2

    //         let result = Escala.create(escalaSave)

    //         console.log( 'Colaborador Folgando -> ' + folgados['nome'] + ' Result -> ' + result)

    //     })

    //     let trabalhadores = colaboradores.filter( (element, index, array) => {
    //         return (parseInt(element['diasFolga']) != dia['numDiaSemana'])
    //     })
        
    //     trabalhadores.forEach(deuRuim => {
            
    //         let escalaSave : {[k: string]: any} = {}

    //         escalaSave.diaId = dia['id']
    //         escalaSave.colaboradorId = deuRuim['id']
    //         escalaSave.tipoEscala = 1

    //         let result = Escala.create(escalaSave)

    //         console.log( 'Colaborador Trabalhando -> ' + deuRuim['nome'] + ' Result -> ' + result)

    //     })

    // }

}

export default new EscalaService()