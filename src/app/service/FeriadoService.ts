import { Feriado } from '../model/FeriadosModel';
import { Data } from './../../util/Data';
import { FactoryDB } from './../../util/FactoryDB';
import { Sequelize } from 'sequelize-typescript';
import { msgFeriado } from './../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import UtilService from '../../util/UtilService';
import { Filial } from '../model/FilialModel';

class FeriadoService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await Feriado.findById(id);

            if(result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgFeriado.nenhumFeriadoEncontrado,
                    'Nenhum feriado encontrado',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFeriado.erroAoObterFeriado, error);
            res.status(500).json(data);
        }       
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            data.addInclude(Filial, "id", "filial_id", null);
            await data.executeQuery(req, res, Feriado);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFeriado.erroAoListarFeriados, error);
            res.status(500).json(data);
        }  
    }

    // Cria um novo registro
    public async create(req, res) {
        let data = new Data();
        let feriado = req.body;
        
        // Campo diaAno não pode ser vazio.
        req.assert('diaAno', msgFeriado.feriadoCampoDiaAnoObrigatorio).notEmpty();
        // Campo diaUtil não pode ser vazio.
        req.assert('diaUtil', msgFeriado.feriadoCampoDiaUtilObrigatorio).notEmpty();
        // Campo filialId não pode ser nulo.
        req.assert('filialId', msgFeriado.feriadoCampoFilialIdObrigatorio).notEmpty();                

        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgFeriado.erroAoInserirFeriado, errors);                
                      
            res.status(400).send(data);
            return;
        }

        try {
            const Op = Sequelize.Op;

            let feriadoEncontrado = await Feriado.findOne({   
                where: { 
                    [Op.and]: {
                        diaAno: feriado.diaAno,
                        filialId: feriado.filialId                        
                    }  
                }
            });

            if(feriadoEncontrado) {
                data.addMsg(tMsg.DANGER, msgFeriado.feriadoDeveSerUnico); 
                res.status(500).send(data);
                return;
            } else {                
                let result = await Feriado.create(feriado);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);                
                res.status(201).send(data);
            }
                        
        } catch (error) {           
            data.addMsgError(tMsg.DANGER, msgFeriado.erroAoInserirFeriado,
                error);
            
            res.status(500).send(data);
        } 
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let feriado = req.body;
        let id = req.params.id;

        let feriadoEncontrado;
        const Op = Sequelize.Op;

        let result;

        // Campo diaAno não pode ser vazio.
        req.assert('diaAno', msgFeriado.feriadoCampoDiaAnoObrigatorio).notEmpty();
        // Campo diaUtil não pode ser vazio.
        req.assert('diaUtil', msgFeriado.feriadoCampoDiaUtilObrigatorio).notEmpty();
        // Campo filialId não pode ser nulo.
        req.assert('filialId', msgFeriado.feriadoCampoFilialIdObrigatorio).notEmpty(); 
        
        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgFeriado.erroAoAtualizarFeriado, errors);
                        
            res.status(400).send(data);
            return;
        }

        let feriadoUnico = await Feriado.findOne({   
            where: { 
                [Op.and]: {
                    diaAno: feriado.diaAno,
                    filialId: feriado.filialId                        
                }  
            }
        });        
        
        if(feriadoUnico !== null && feriadoUnico.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgFeriado.erroAoAtualizarFeriado, 
                msgFeriado.feriadoDeveSerUnico, "diaAno", feriado.diaAno); 
            res.status(500).send(data);
            return;
        }

        try {
            feriadoEncontrado = await Feriado.findById(id);

            if(!feriadoEncontrado) {
                data.addMsg(tMsg.DANGER, msgFeriado.nenhumFeriadoEncontrado);
                res.status(500).send(data);
                return;

            } else { 
                result = await Feriado.update(feriado, {
                    where: {
                        id: id
                    }
                });

                if(result > 0) {
                    data.obj = feriado;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgFeriado.erroAoAtualizarFeriado);
                    res.status(500).send(data);
                }
                
                return;
            }
            
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFeriado.erroAoAtualizarFeriado, error);
            res.status(500).send(data);
        }        
    }

    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let feriadoEncontrado = await Feriado.findById(id);           

            if(feriadoEncontrado) {
                result = await Feriado.destroy({
                    where: {
                        id: id
                    }
                });               

                if(result > 0) {
                    res.status(200).send(feriadoEncontrado);

                } else {
                    data.addMsg(tMsg.DANGER, msgFeriado.erroAoRemoverFeriado);
                    res.status(500).send(feriadoEncontrado);
                }                

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgFeriado.nenhumFeriadoEncontrado,
                    'Nenhum feriado encontrado',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFeriado.erroAoRemoverFeriado, error);
            res.status(500).send(data);
        }
    }
}

export default new FeriadoService();