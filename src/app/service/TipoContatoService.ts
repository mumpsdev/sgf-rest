import { TipoContato } from '../model/TipoContatoModel';
import { Data } from './../../util/Data';
import { FactoryDB } from './../../util/FactoryDB';
import { Sequelize } from 'sequelize-typescript';
import { msgTipoContato } from './../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import UtilService from '../../util/UtilService';

class TipoContatoService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await TipoContato.findById(id);

            if(result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgTipoContato.nenhumTipoContatoEncontrado,
                    'Nenhum Tipo de Contato encontrado',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoContato.erroAoObterTipoContato, error);
            res.status(500).json(data);
        }       
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {                       
            await data.executeQuery(req, res, TipoContato);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoContato.erroAoListarTipoContato, error);
            res.status(500).json(data);
        }
    }

    // Cria um novo registro
    public async create(req, res) {
        let data = new Data();
        let tipoContato = req.body;
        
        // Campo nome deve conter no mín 2 e no máx 30 caracteres.
        req.assert('nome', msgTipoContato.tipoContatoCampoNomeRangeObrigatorio).len(2, 30);
        // Campo tipo não pode ser vazio nem ser diferente dos campos listados.
        req.assert('tipo', msgTipoContato.tipoContatoCampoTipoInvalido).notEmpty().isIn(['string', 'number', 'boolean', 'date']);
        // Campo mask não pode ser nulo.
        req.assert('mask', msgTipoContato.tipoContatoCampoMaskInvalido).notEmpty();
        // Campo regex não pode ser nulo.
        req.assert('regex', msgTipoContato.tipoContatoCampoRegexInvalido).notEmpty();
        // Campo qtMinCaracters não pode ser nulo e deve ser numérico.
        req.assert('qtMinCaracters', msgTipoContato.tipoContatoCampoQtMinCaractersInvalido).notEmpty().matches(/\d/);
        // Campo qtMaxCaracters não pode ser nulo e deve ser numérico.
        // Campo qtMaxCaracters deve ser maior que o campo qtMinCaracters.
        req.assert('qtMaxCaracters', msgTipoContato.tipoContatoCampoQtMaxCaractersInvalido).notEmpty().matches(/\d/)
            .isInt({ gt : req.body.qtMinCaracters});       

        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgTipoContato.erroAoInserirTipoContato, errors);                
                      
            res.status(400).send(data);
            return;
        }

        try {
            let tipoContatoEncontrado = await TipoContato.findOne({ where: { nome: tipoContato.nome }});

            if(tipoContatoEncontrado) {
                data.addMsg(tMsg.DANGER, msgTipoContato.tipoContatoDeveSerUnico); 
                res.status(500).send(data);
                return;
            } else {                
                let result = await TipoContato.create(tipoContato);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);                
                res.status(201).send(data);
            }
                        
        } catch (error) {           
            data.addMsgError(tMsg.DANGER, msgTipoContato.erroAoInserirTipoContato,
                error);
            
            res.status(500).send(data);
        } 
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let tipoContato = req.body;
        let id = req.params.id;

        let tipoContatoEncontrado;
        const Op = Sequelize.Op;

        let result;

        // Campo nome deve conter no mín 2 e no máx 30 caracteres.
        req.assert('nome', msgTipoContato.tipoContatoCampoNomeRangeObrigatorio).len(2, 30);
        // Campo tipo não pode ser vazio nem ser diferente dos campos listados.
        req.assert('tipo', msgTipoContato.tipoContatoCampoTipoInvalido).notEmpty().isIn(['string', 'number', 'boolean', 'date']);
        // Campo mask não pode ser nulo.
        req.assert('mask', msgTipoContato.tipoContatoCampoMaskInvalido).notEmpty();
        // Campo regex não pode ser nulo.
        req.assert('regex', msgTipoContato.tipoContatoCampoRegexInvalido).notEmpty();
        // Campo qtMinCaracters não pode ser nulo e deve ser numérico.
        req.assert('qtMinCaracters', msgTipoContato.tipoContatoCampoQtMinCaractersInvalido).notEmpty().matches(/\d/);
        // Campo qtMaxCaracters não pode ser nulo e deve ser numérico.
        // Campo qtMaxCaracters deve ser maior que o campo qtMinCaracters.
        req.assert('qtMaxCaracters', msgTipoContato.tipoContatoCampoQtMaxCaractersInvalido).notEmpty().matches(/\d/)
            .isInt({ gt : req.body.qtMinCaracters});  
        
        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgTipoContato.erroAoAtualizarTipoContato, errors);
                        
            res.status(400).send(data);
            return;
        }

        let tipoContatoUnico = await TipoContato.findOne({ where: { nome: tipoContato.nome }});
        
        if(tipoContatoUnico !== null && tipoContatoUnico.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgTipoContato.erroAoAtualizarTipoContato, 
                msgTipoContato.tipoContatoDeveSerUnico, "nome", tipoContato.nome); 
            res.status(500).send(data);
            return;
        }

        try {
            tipoContatoEncontrado = await TipoContato.findById(id);

            if(!tipoContatoEncontrado) {
                data.addMsg(tMsg.DANGER, msgTipoContato.nenhumTipoContatoEncontrado);
                res.status(500).send(data);
                return;

            } else { 
                tipoContato.dataAtualizacao = new Date();
                
                result = await TipoContato.update(tipoContato, {
                    where: {
                        id: id
                    }
                });

                if(result > 0) {
                    data.obj = tipoContato;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgTipoContato.erroAoAtualizarTipoContato);
                    res.status(500).send(data);
                }
                
                return;
            }
            
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoContato.erroAoAtualizarTipoContato, error);
            res.status(500).send(data);
        }        
    }

    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let tipoContatoEncontrado = await TipoContato.findById(id);           

            if(tipoContatoEncontrado) {
                result = await TipoContato.update({ativo: false}, {
                    where: {
                        id: id
                    }
                });               

                if(result > 0) {
                    res.status(200).send(tipoContatoEncontrado);

                } else {
                    data.addMsg(tMsg.DANGER, msgTipoContato.erroAoRemoverTipoContato);
                    res.status(500).send(tipoContatoEncontrado);
                }                

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgTipoContato.nenhumTipoContatoEncontrado,
                    'Nenhum tipo contato encontrado',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgTipoContato.erroAoRemoverTipoContato, error);
            res.status(500).send(data);
        }
    }
}

export default new TipoContatoService();