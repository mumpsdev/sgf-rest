import { Perfil } from '../model/PerfilModel';
import { Data } from './../../util/Data';
import { FactoryDB } from './../../util/FactoryDB';
import { Sequelize } from 'sequelize-typescript';
import { msgPerfil } from './../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import UtilService from '../../util/UtilService';
import { PerfilAcao } from '../model/PerfilAcaoModel';
import { Acao } from '../model/AcaoModel';
import App from "./../../app";


class PerfilService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await Perfil.findOne({
                attributes: ['id', 'nome'],
                include: [{
                    attributes: ['acaoId'],
                    model: PerfilAcao, as: "perfilAcao",
                    include: [{
                        attributes: ['id', 'nome', 'url', 'metodo'],
                        model: Acao, as: "acao",
                        where: {
                            metodo: {
                                [Op.notIn]: ["NONE"]
                            }
                        }
                    }]
                }],
                where: {
                    id: id
                }
            });

            if (result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER,
                    msgPerfil.nenhumPerfilEncontrado,
                    'Nenhum perfil encontrado',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgPerfil.erroAoObterPerfil, error);
            res.status(500).json(data);
        }
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {            
            await data.executeQuery(req, res, Perfil);            
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgPerfil.erroAoListarPerfis, error);
            res.status(500).json(data);
        }
    }

    // Cria um novo perfil
    public async create(req, res) {
        let data = new Data();
        let perfil = req.body;

        // Campo nome deve conter no mín 3 e no máx 30 caracteres.
        req.assert('nome', msgPerfil.perfilCampoNomeRangeObrigatorio).len(3, 30);

        // Campo perfilAcao não poder ser nulo nem vazio.
        req.assert('perfilAcao', msgPerfil.perfilListaAcoesObrigatorio).notEmpty();

        let errors = req.validationErrors();

        if (errors) {
            data.addMsgError(tMsg.DANGER, msgPerfil.erroAoInserirPerfil, errors);

            res.status(400).send(data);
            return;
        }

        try {
            let perfilEncontrado = await Perfil.findOne({ where: { nome: perfil.nome } });

            if (perfilEncontrado) {
                data.addMsg(tMsg.DANGER, msgPerfil.perfilDeveSerUnico);
                res.status(500).send(data);
                return;
            } else {
                let t = await App.getSequelize().transaction();
                
                let result = await Perfil.create(perfil, {                    
                    include: [{
                        model: PerfilAcao,
                        as: 'perfilAcao',                        
                    }],
                    transaction: t                    
                });
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);
                res.status(201).send(data);
            }

        } catch (error) {
            await PerfilAcao.destroy({ where: { perfilId: perfil.id }});
            data.addMsgError(tMsg.DANGER, msgPerfil.erroAoInserirPerfil,
                error);

            res.status(500).send(data);
        }
    }

    // Atualiza um perfil existente.
    public async update(req, res) {
        let data = new Data();

        let perfil = req.body;
        let id = req.params.id;

        let perfilEncontrado;
        const Op = Sequelize.Op;

        let result;

        // Campo nome deve conter no mín 3 e no máx 30 caracteres.
        req.assert('nome', msgPerfil.perfilCampoNomeRangeObrigatorio).len(3, 30);

        // Campo perfilAcao não poder ser nulo nem vazio.
        req.assert('perfilAcao', msgPerfil.perfilListaAcoesObrigatorio).notEmpty();

        let errors = req.validationErrors();

        if (errors) {
            data.addMsgError(tMsg.DANGER, msgPerfil.erroAoAtualizarPerfil, errors);

            res.status(400).send(data);
            return;
        }

        let perfilUnico = await Perfil.findOne({ where: { nome: perfil.nome } });

        if (perfilUnico && perfilUnico.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgPerfil.erroAoAtualizarPerfil,
                msgPerfil.perfilDeveSerUnico, "nome", perfil.nome);
            res.status(500).send(data);
            return;
        }

        try {
            perfilEncontrado = await Perfil.findById(id); 

            if(perfilEncontrado) {                    
                result = await Perfil.update(perfil, { // Atualiza atributos do perfil
                    where: {
                        id: id
                    }
                });

                if (result > 0) { // Perfil atualizado com sucesso
                    await PerfilAcao.destroy({ // Remove ocorrências de perfilAcao associados ao id do perfil
                        where: {
                            perfilId: id
                        }
                    });

                    let perfisAcao: Array<PerfilAcao> = perfil.perfilAcao.slice();

                    perfisAcao.forEach(perfilAcao => { // Itera os perfisAcao e adiciona um a um
                        perfilAcao.perfilId = id;
                        PerfilAcao.create(perfilAcao);
                    });
                }

                data.obj = perfil;
                res.status(200).send(data);

                return;

            } else {
                data.addMsg(tMsg.DANGER, msgPerfil.nenhumPerfilEncontrado);
                res.status(500).send(data);
                return;
            }             
            
        } catch(error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoAtualizarPerfil);
            res.status(500).send(data);
        }        
    }

    // Desabilita um perfil existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let perfilEncontrado = await Perfil.findById(id);

            if (perfilEncontrado) {
                result = await Perfil.update({ ativo: false }, {
                    where: {
                        id: id
                    }
                });

                if (result > 0) {
                    res.status(200).send(result);

                } else {
                    data.addMsg(tMsg.DANGER, msgPerfil.erroAoRemoverPerfil);
                    res.status(500).send(perfilEncontrado);
                }

            } else {
                data.addMsgWithDetails(tMsg.DANGER,
                    msgPerfil.nenhumPerfilEncontrado,
                    'Nenhum perfil encontrado',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgPerfil.erroAoRemoverPerfil, error);
            res.status(500).send(data);
        }
    }
}

export default new PerfilService();