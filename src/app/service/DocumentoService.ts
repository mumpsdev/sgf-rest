import { Documento } from '../model/DocumentoModel';
import { Data } from './../../util/Data';
import { FactoryDB } from './../../util/FactoryDB';
import { Sequelize } from 'sequelize-typescript';
import { msgDocumento } from './../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import UtilService from '../../util/UtilService';
import { TipoDocumento } from '../model/TipoDocumentoModel';

class DocumentoService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await Documento.findById(id);

            if (result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER,
                    msgDocumento.nenhumDocumentoEncontrado,
                    'Nenhum documento encontrado',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgDocumento.erroAoObterDocumento, error);
            res.status(500).json(data);
        }
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            data.addInclude(TipoDocumento, "id", "tipo_documento_id", null);
            await data.executeQuery(req, res, Documento);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgDocumento.erroAoListarDocumento, error);
            res.status(500).json(data);
        }
    }

    // Cria um novo registro
    public async create(req, res) {
        let data = new Data();
        let documento = req.body;

        // Campo valor não pode ser vazio.
        req.assert('valor', msgDocumento.documentoCampoValorRangeObrigatorio).notEmpty();
        // Campo tipo documento não pode ser vazio.
        req.assert('tipoDocumentoId', msgDocumento.documentoCampoTipoInvalido).notEmpty();
        // Campo chave grupo não pode ser nulo.
        req.assert('chaveGrupo', msgDocumento.documentoCampoChaveGrupoInvalido).notEmpty();

        let errors = req.validationErrors();

        if (errors) {
            data.addMsgError(tMsg.DANGER, msgDocumento.erroAoInserirDocumento, errors);

            res.status(400).send(data);
            return;
        }

        try {
            let documentoEncontrado = await Documento.findOne({ where: { valor: documento.valor } });

            if (documentoEncontrado) {
                data.addMsg(tMsg.DANGER, msgDocumento.documentoDeveSerUnico);
                res.status(500).send(data);
                return;
            } else {
                let tipoDocumento = await TipoDocumento.findById(documento.tipoDocumentoId);

                if (!tipoDocumento) {
                    data.addMsgError(tMsg.DANGER,
                        msgDocumento.documentoAssociacaoTipoDocumentoNaoEncontrado,
                        'Tipo Documento de id ' + documento.tipoDocumentoId + ' não cadastrado.');
                    res.status(500).send(data);
                    return;
                }

                let result = await Documento.create(documento);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);
                res.status(201).send(data);
            }

        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgDocumento.erroAoInserirDocumento,
                error);

            res.status(500).send(data);
        }
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let documento = req.body;
        let id = req.params.id;

        let documentoEncontrado;
        const Op = Sequelize.Op;

        let result;

        // Campo valor não pode ser vazio.
        req.assert('valor', msgDocumento.documentoCampoValorRangeObrigatorio).notEmpty();
        // Campo tipo documento não pode ser vazio.
        req.assert('tipoDocumentoId', msgDocumento.documentoCampoTipoInvalido).notEmpty();
        // Campo chave grupo não pode ser nulo.
        req.assert('chaveGrupo', msgDocumento.documentoCampoChaveGrupoInvalido).notEmpty();

        let errors = req.validationErrors();

        if (errors) {
            data.addMsgError(tMsg.DANGER, msgDocumento.erroAoAtualizarDocumento, errors);

            res.status(400).send(data);
            return;
        }

        let documentoUnico = await Documento.findOne({ where: { valor: documento.valor } });

        if (documentoUnico !== null && documentoUnico.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgDocumento.erroAoAtualizarDocumento,
                msgDocumento.documentoDeveSerUnico, "valor", documento.valor);
            res.status(500).send(data);
            return;
        }

        try {
            documentoEncontrado = await Documento.findById(id);

            if (!documentoEncontrado) {
                data.addMsg(tMsg.DANGER, msgDocumento.nenhumDocumentoEncontrado);
                res.status(500).send(data);
                return;

            } else {
                documento.dataAtualizacao = new Date();

                result = await Documento.update(documento, {
                    where: {
                        id: id
                    }
                });

                if (result > 0) {
                    data.obj = documento;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgDocumento.erroAoAtualizarDocumento);
                    res.status(500).send(data);
                }

                return;
            }

        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgDocumento.erroAoAtualizarDocumento, error);
            res.status(500).send(data);
        }
    }

    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let documentoEncontrado = await Documento.findById(id);

            if (documentoEncontrado) {
                result = await Documento.update({ ativo: false }, {
                    where: {
                        id: id
                    }
                });

                if (result > 0) {
                    res.status(200).send(documentoEncontrado);

                } else {
                    data.addMsg(tMsg.DANGER, msgDocumento.erroAoRemoverDocumento);
                    res.status(500).send(documentoEncontrado);
                }

            } else {
                data.addMsgWithDetails(tMsg.DANGER,
                    msgDocumento.nenhumDocumentoEncontrado,
                    'Nenhum documento encontrado',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgDocumento.erroAoRemoverDocumento, error);
            res.status(500).send(data);
        }
    }
}

export default new DocumentoService();