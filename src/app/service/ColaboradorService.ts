import { Colaborador } from './../model/ColaboradorModel';
import { Data } from './../../util/Data';
import { msgColaborador } from './../../util/MessageTranslate';
import { oMsg, tMsg } from './../../util/Values';
import { FilialPerfil } from '../model/FilialPerfilModel';
import { Perfil } from '../model/PerfilModel';
import { Filial } from '../model/FilialModel';
import { Model } from 'sequelize-typescript/lib/models/Model';
import { Sequelize } from 'sequelize-typescript';
import UtilService from '../../util/UtilService';

class ColaboradorService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await Colaborador.findById(id);

            if(result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgColaborador.nenhumColaboradorEncontrado,
                    'Nenhum colaborador encontrado',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoObterColaborador, error);
            res.status(500).json(data);
        }       
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            await data.executeQuery(req, res, Colaborador);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoListarColaborador, error);
            res.status(500).json(data);
        } 
    }

     // Cria um novo registro
     public async create(req, res) {
        let data = new Data();
        let colaborador = req.body;
        
        // Campo nome não pode ser vazio.
        req.assert('nome', msgColaborador.colaboradorCampoNomeObrigatorio).notEmpty();
        // Campo login não pode ser vazio.
        req.assert('login', msgColaborador.colaboradorCampoLoginObrigatorio).notEmpty();
        // Campo password não pode ser nulo.
        req.assert('password', msgColaborador.colaboradorCampoPasswordObrigatorio).notEmpty();        
        // Campo chave grupo documento não pode ser nulo.
        req.assert('chaveGrupoDocumento', msgColaborador.colaboradorCampoChaveGrupoDocumentoObrigatorio).notEmpty();        
        // Campo chave grupo não pode ser nulo.
        req.assert('chaveGrupoContato', msgColaborador.colaboradorCampoChaveGrupoContatoObrigatorio).notEmpty();        

        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoInserirColaborador, errors);                
                      
            res.status(400).send(data);
            return;
        }

        try {
            let contatoEncontrado = await Colaborador.findOne({ where: { nome: colaborador.nome }});

            if(contatoEncontrado) {
                data.addMsg(tMsg.DANGER, msgColaborador.colaboradorDeveSerUnico); 
                res.status(500).send(data);
                return;
            } else {                
                let result = await Colaborador.create(colaborador);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);                
                res.status(201).send(data);
            }
                        
        } catch (error) {           
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoInserirColaborador,
                error);
            
            res.status(500).send(data);
        } 
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let colaborador = req.body;
        let id = req.params.id;

        let colaboradorEncontrado;
        const Op = Sequelize.Op;

        let result;

        // Campo nome não pode ser vazio.
        req.assert('nome', msgColaborador.colaboradorCampoNomeObrigatorio).notEmpty();
        // Campo login não pode ser vazio.
        req.assert('login', msgColaborador.colaboradorCampoLoginObrigatorio).notEmpty();
        // Campo chave grupo não pode ser nulo.
        req.assert('password', msgColaborador.colaboradorCampoPasswordObrigatorio).notEmpty();  
        // Campo chave grupo documento não pode ser nulo.
        req.assert('chaveGrupoDocumento', msgColaborador.colaboradorCampoChaveGrupoDocumentoObrigatorio).notEmpty();        
        // Campo chave grupo não pode ser nulo.
        req.assert('chaveGrupoContato', msgColaborador.colaboradorCampoChaveGrupoContatoObrigatorio).notEmpty();        
        
        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoAtualizarColaborador, errors);
                        
            res.status(400).send(data);
            return;
        }

        let colaboradorUnico = await Colaborador.findOne({ where: { nome: colaborador.nome }});        
        
        if(colaboradorUnico !== null && colaboradorUnico.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgColaborador.erroAoAtualizarColaborador, 
                msgColaborador.colaboradorDeveSerUnico, "nome", colaborador.nome); 
            res.status(500).send(data);
            return;
        }

        try {
            colaboradorEncontrado = await Colaborador.findById(id);

            if(!colaboradorEncontrado) {
                data.addMsg(tMsg.DANGER, msgColaborador.nenhumColaboradorEncontrado);
                res.status(500).send(data);
                return;

            } else { 
                colaborador.dataAtualizacao = new Date();
                
                result = await Colaborador.update(colaborador, {
                    where: {
                        id: id
                    }
                });

                if(result > 0) {
                    data.obj = colaborador;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgColaborador.erroAoAtualizarColaborador);
                    res.status(500).send(data);
                }
                
                return;
            }
            
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoAtualizarColaborador, error);
            res.status(500).send(data);
        }        
    }

    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let colaboradorEncontrado = await Colaborador.findById(id);           

            if(colaboradorEncontrado) {
                result = await Colaborador.update({status: "4"}, {
                    where: {
                        id: id
                    }
                });               

                if(result > 0) {
                    res.status(200).send(colaboradorEncontrado);

                } else {
                    data.addMsg(tMsg.DANGER, msgColaborador.erroAoRemoverColaborador);
                    res.status(500).send(colaboradorEncontrado);
                }                

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgColaborador.nenhumColaboradorEncontrado,
                    'Nenhum colaborador encontrado',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoRemoverColaborador, error);
            res.status(500).send(data);
        }
    }   

    /**
     * Serviço responsável por retornar as filiais do colaborador logado.
     * 
     * @param req  
     * @param res 
     */
    public async getFiliais(req, res) {
        let data = new Data();

        let userLogged = req["login"];
        let loginUserSession = userLogged["login"]; 
        
        const Op = Sequelize.Op;

        try {
            let filiais = await Filial.findAll({
                attributes: ['id', 'fantasia', 'razaoSocial'],
                include: [{ 
                        attributes: [],                        
                        model: FilialPerfil, as: "filialPerfil",                         
                        include: [{                            
                            model: Colaborador, as: "colaborador",
                            where: {
                                "login": loginUserSession
                            }
                        }]                       
                    }
                ],
                where: {
                    [Op.and]: {
                        status: '2' // Status 2 corresponde a status 'ativo'
                    }
                }
            });               
    
            data.list = filiais;
    
            res.status(200).json(data);

        } catch(error) {
            data.addMsgError(tMsg.DANGER, msgColaborador.erroAoObterFiliaisDoColaboradorAutenticado, error);
            res.status(500).json(data);
        }
    }
}

export default new ColaboradorService();