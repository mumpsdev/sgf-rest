import { Sequelize } from 'sequelize-typescript';
import { oMsg, tMsg } from './../../util/Values';
import { Data } from '../../util/Data';
import { msgAcao } from '../../util/MessageTranslate';
import { Acao } from '../model/AcaoModel';


class AcaoService{

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            await data.executeQuery(req, res, Acao);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgAcao.erroAoListarAcoes, error);
            res.status(500).json(data);
        }
    }

    public async listNotIn(req, res) {
        let data = new Data();
        let Op = Sequelize.Op;
        let query = {};
        let notin = [];
        try {
            if(req.query.notin){
                notin = req.query.notin && req.query.notin.indexOf(",") > -1 ? req.query.notin.split(',') : req.query.notin;
                notin = [].concat( notin);
                console.log(notin);
            }
            let where = {
                [Op.and]: {
                    id: {[Op.notIn]: notin},
                    "metodo": {[Op.notIn]: ["NONE"]}
                }
            };
            query["where"] = where;
            let result = await Acao.findAndCountAll(query);
            data.list = result.rows;
            data.qtdTotal = result.count;
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgAcao.erroAoListarAcoes, error);
            res.status(500).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            let acao = req.body;
            // data.obj = await AcaoDAO.daoMongo.create(acao);
            data.addMsg(tMsg.SUCCESS, msgAcao.acaoInseridaComSucesso);
            
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoInserirAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            let acao = req.body;
            // data.obj = await AcaoDAO.daoMongo.update(id, acao);
            data.addMsg(tMsg.SUCCESS, msgAcao.acaoAtualizadaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoAtualizarAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            // data.obj = await AcaoDAO.daoMongo.findByIdAndRemove(id);
            data.addMsg(tMsg.SUCCESS, msgAcao.acaoRemovidaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoRemoverAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            // data.obj = await AcaoDAO.daoMongo.findById(id);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoLocalizarAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }
}

export default new AcaoService();