import { Data } from '../../util/Data';
import { msgFilial } from '../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import { Sequelize } from 'sequelize-typescript';
import { Filial } from '../model/FilialModel';
import { FilialPerfil } from '../model/FilialPerfilModel';
import UtilService from '../../util/UtilService';
import { Organizacao } from '../model/OrganizacaoModel';
import { Colaborador } from '../model/ColaboradorModel';

class FilialService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await Filial.findById(id);

            if (result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER,
                    msgFilial.nenhumaFilialEncontrada,
                    'Nenhuma filial encontrada',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFilial.erroAoObterFilial, error);
            res.status(500).json(data);
        }
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            data.addInclude(Organizacao, "id", "organizacao_id", null);
            await data.executeQuery(req, res, Filial);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFilial.erroAoListarFiliais, error);
            res.status(500).json(data);
        }
    }

    // Cria um novo registro
    public async create(req, res) {
        let data = new Data();
        let filial = req.body;

        // Campo fantasia não pode ser vazio.
        req.assert('fantasia', msgFilial.filialCampoFantasiaObrigatorio).notEmpty();
        // Campo razão social não pode ser vazio.
        req.assert('razaoSocial', msgFilial.filialCampoRazaoSocialObrigatorio).notEmpty();
        // Campo organizacao não pode ser vazio.
        req.assert('organizacaoId', msgFilial.filialCampoOrganizacaoObrigatorio).notEmpty();
        // Campo chave grupo contato não pode ser nulo.
        req.assert('chaveGrupoContato', msgFilial.filialCampoChaveGrupoContatoObrigatorio).notEmpty();
        // Campo chave grupo documento não pode ser nulo.
        req.assert('chaveGrupoDocumento', msgFilial.filialCampoChaveGrupoDocumentoObrigatorio).notEmpty();

        let errors = req.validationErrors();

        if (errors) {
            data.addMsgError(tMsg.DANGER, msgFilial.erroAoInserirFilial, errors);

            res.status(400).send(data);
            return;
        }

        try {
            let filialEncontrada = await Filial.findOne({ where: { fantasia: filial.fantasia } });

            if (filialEncontrada) {
                data.addMsg(tMsg.DANGER, msgFilial.filialDeveSerUnica);
                res.status(500).send(data);
                return;

            } else {
                let organizacao = await Organizacao.findById(filial.organizacaoId);

                if (!organizacao) {
                    data.addMsgError(tMsg.DANGER,
                        msgFilial.filialAssociacaoOrganizacaoNaoEncontrada,
                        'Organização de id ' + filial.organizacaoId + ' não cadastrada.');
                    res.status(500).send(data);
                    return;
                }

                let result = await Filial.create(filial);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);
                res.status(201).send(data);
            }

        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFilial.erroAoInserirFilial,
                error);

            res.status(500).send(data);
        }
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let filial = req.body;
        let id = req.params.id;

        let filialEncontrada;
        const Op = Sequelize.Op;

        let result;

        // Campo fantasia não pode ser vazio.
        req.assert('fantasia', msgFilial.filialCampoFantasiaObrigatorio).notEmpty();
        // Campo razão social não pode ser vazio.
        req.assert('razaoSocial', msgFilial.filialCampoRazaoSocialObrigatorio).notEmpty();
        // Campo organizacao não pode ser vazio.
        req.assert('organizacaoId', msgFilial.filialCampoOrganizacaoObrigatorio).notEmpty();
        // Campo chave grupo contato não pode ser nulo.
        req.assert('chaveGrupoContato', msgFilial.filialCampoChaveGrupoContatoObrigatorio).notEmpty();
        // Campo chave grupo documento não pode ser nulo.
        req.assert('chaveGrupoDocumento', msgFilial.filialCampoChaveGrupoDocumentoObrigatorio).notEmpty();

        let errors = req.validationErrors();

        if (errors) {
            data.addMsgError(tMsg.DANGER, msgFilial.erroAoAtualizarFilial, errors);

            res.status(400).send(data);
            return;
        }

        let filialUnica = await Filial.findOne({ where: { fantasia: filial.fantasia } });

        if (filialUnica !== null && filialUnica.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgFilial.erroAoAtualizarFilial,
                msgFilial.filialDeveSerUnica, "fantasia", filial.fantasia);
            res.status(500).send(data);
            return;
        }

        try {
            filialEncontrada = await Filial.findById(id);

            if (!filialEncontrada) {
                data.addMsg(tMsg.DANGER, msgFilial.nenhumaFilialEncontrada);
                res.status(500).send(data);
                return;

            } else {
                let organizacao = await Organizacao.findById(filial.organizacaoId);

                if (!organizacao) {
                    data.addMsgError(tMsg.DANGER,
                        msgFilial.filialAssociacaoOrganizacaoNaoEncontrada,
                        'Organização de id ' + filial.organizacaoId + ' não cadastrada.');
                    res.status(500).send(data);
                    return;
                }

                filial.dataAtualizacao = new Date();

                result = await Filial.update(filial, {
                    where: {
                        id: id
                    }
                });

                if (result > 0) {
                    data.obj = filial;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgFilial.erroAoAtualizarFilial);
                    res.status(500).send(data);
                }

                return;
            }

        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFilial.erroAoAtualizarFilial, error);
            res.status(500).send(data);
        }
    }

    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let filialEncontrada = await Filial.findById(id);

            if (filialEncontrada) {
                result = await Filial.update({ status: 4 }, {
                    where: {
                        id: id
                    }
                });

                if (result > 0) {
                    res.status(200).send(filialEncontrada);

                } else {
                    data.addMsg(tMsg.DANGER, msgFilial.erroAoRemoverFilial);
                    res.status(500).send(filialEncontrada);
                }

            } else {
                data.addMsgWithDetails(tMsg.DANGER,
                    msgFilial.nenhumaFilialEncontrada,
                    'Nenhuma filial encontrada',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgFilial.erroAoRemoverFilial, error);
            res.status(500).send(data);
        }
    }
}

export default new FilialService();