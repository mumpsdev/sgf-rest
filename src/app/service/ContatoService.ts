import { TipoContato } from './../model/TipoContatoModel';
import { Contato } from '../model/ContatoModel';
import { Data } from './../../util/Data';
import { FactoryDB } from './../../util/FactoryDB';
import { Sequelize } from 'sequelize-typescript';
import { msgContato } from './../../util/MessageTranslate';
import { oMsg, tMsg } from '../../util/Values';
import UtilService from '../../util/UtilService';

class ContatoService {

    // Busca apenas um resultado.
    public async get(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            result = await Contato.findById(id);

            if(result) {
                data.obj = result;
                res.status(200).json(data);

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgContato.nenhumContatoEncontrado,
                    'Nenhum contato encontrado',
                    'id',
                    id);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgContato.erroAoObterContato, error);
            res.status(500).json(data);
        }       
    }

    // Pode trazer uma lista de resultados paginados e filtrados.
    public async list(req, res) {
        let data = new Data();
        try {
            data.addInclude(TipoContato, "id", "tipo_contato_id", null);
            await data.executeQuery(req, res, Contato);
            res.status(200).json(data);
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgContato.erroAoListarContato, error);
            res.status(500).json(data);
        }  
    }

    // Cria um novo registro
    public async create(req, res) {
        let data = new Data();
        let contato = req.body;
        
        // Campo valor não pode ser vazio.
        req.assert('valor', msgContato.contatoCampoValorRangeObrigatorio).notEmpty();
        // Campo tipo contato não pode ser vazio.
        req.assert('tipoContatoId', msgContato.contatoCampoTipoInvalido).notEmpty();
        // Campo chave grupo não pode ser nulo.
        req.assert('chaveGrupo', msgContato.contatoCampoChaveGrupoInvalido).notEmpty();        

        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgContato.erroAoInserirContato, errors);                
                      
            res.status(400).send(data);
            return;
        }

        try {
            let contatoEncontrado = await Contato.findOne({ where: { valor: contato.valor }});

            if(contatoEncontrado) {
                data.addMsg(tMsg.DANGER, msgContato.contatoDeveSerUnico); 
                res.status(500).send(data);
                return;
            } else {                
                let tipoContato = await TipoContato.findById(contato.tipoContatoId);

                if(!tipoContato) {
                    data.addMsgError(tMsg.DANGER, 
                        msgContato.contatoAssociacaoTipoContatoNaoEncontrado,
                        'Tipo Contato de id ' + contato.tipoContatoId + ' não cadastrado.'); 
                    res.status(500).send(data);
                    return;    
                }

                let result = await Contato.create(contato);
                data.obj = result;
                res.location(UtilService.fullUrl(req) + '/' + result.id);                
                res.status(201).send(data);
            }
                        
        } catch (error) {           
            data.addMsgError(tMsg.DANGER, msgContato.erroAoInserirContato,
                error);
            
            res.status(500).send(data);
        } 
    }

    // Atualiza um registro existente.
    public async update(req, res) {
        let data = new Data();

        let contato = req.body;
        let id = req.params.id;

        let contatoEncontrado;
        const Op = Sequelize.Op;

        let result;

         // Campo valor não pode ser vazio.
        req.assert('valor', msgContato.contatoCampoValorRangeObrigatorio).notEmpty();
        // Campo tipo contato não pode ser vazio.
        req.assert('tipoContatoId', msgContato.contatoCampoTipoInvalido).notEmpty();
        // Campo chave grupo não pode ser nulo.
        req.assert('chaveGrupo', msgContato.contatoCampoChaveGrupoInvalido).notEmpty();  
        
        let errors = req.validationErrors();
        
        if (errors) { 
            data.addMsgError(tMsg.DANGER, msgContato.erroAoAtualizarContato, errors);
                        
            res.status(400).send(data);
            return;
        }

        let contatoUnico = await Contato.findOne({ where: { valor: contato.valor }});        
        
        if(contatoUnico !== null && contatoUnico.id !== parseInt(id)) {
            data.addMsgWithDetails(tMsg.DANGER, msgContato.erroAoAtualizarContato, 
                msgContato.contatoDeveSerUnico, "valor", contato.valor); 
            res.status(500).send(data);
            return;
        }

        try {
            contatoEncontrado = await Contato.findById(id);

            if(!contatoEncontrado) {
                data.addMsg(tMsg.DANGER, msgContato.nenhumContatoEncontrado);
                res.status(500).send(data);
                return;

            } else { 
                contato.dataAtualizacao = new Date();
                
                result = await Contato.update(contato, {
                    where: {
                        id: id
                    }
                });

                if(result > 0) {
                    data.obj = contato;
                    res.status(200).send(data);

                } else {
                    data.addMsg(tMsg.DANGER, msgContato.erroAoAtualizarContato);
                    res.status(500).send(data);
                }
                
                return;
            }
            
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgContato.erroAoAtualizarContato, error);
            res.status(500).send(data);
        }        
    }

    // Desabilita um registro existente.
    public async delete(req, res) {
        let data = new Data();
        let id = req.params.id;

        let result;
        const Op = Sequelize.Op;

        try {
            let contatoEncontrado = await Contato.findById(id);           

            if(contatoEncontrado) {
                result = await Contato.update({ativo: false}, {
                    where: {
                        id: id
                    }
                });               

                if(result > 0) {
                    res.status(200).send(contatoEncontrado);

                } else {
                    data.addMsg(tMsg.DANGER, msgContato.erroAoRemoverContato);
                    res.status(500).send(contatoEncontrado);
                }                

            } else {
                data.addMsgWithDetails(tMsg.DANGER, 
                    msgContato.nenhumContatoEncontrado,
                    'Nenhum contato encontrado',
                    'id',
                    id);
                res.status(404).send(data);
            }
        } catch (error) {
            data.addMsgError(tMsg.DANGER, msgContato.erroAoRemoverContato, error);
            res.status(500).send(data);
        }
    }
}

export default new ContatoService();