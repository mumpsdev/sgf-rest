class FilterService {

    constructor() {
    }

    public async filterPath(req, res, next){
        if (!req.originalUrl.includes("/api/v")) {
            res.redirect('/')
            return
        }
        next()
    }

}

export default new FilterService()