import { Colaborador } from '../model/ColaboradorModel';
import { msgAuth } from './../../util/MessageTranslate';
import { tMsg, oMsg, secretToken } from './../../util/Values';
import UtilService from "../../util/UtilService"
import * as jwt from "jsonwebtoken";
import { Data } from './../../util/Data';
import { Model } from "sequelize-typescript";
import { TipoDocumento } from '../model/TipoDocumentoModel';
import { Filial } from '../model/FilialModel';
import { Documento } from '../model/DocumentoModel';
import { Contato } from '../model/ContatoModel';
import { TipoContato } from '../model/TipoContatoModel';
import { Perfil } from '../model/PerfilModel';
import { FilialPerfil } from '../model/FilialPerfilModel';
import { Acao } from '../model/AcaoModel';
import { PerfilAcao } from '../model/PerfilAcaoModel';
import { PermissaoAcao } from '../model/PermissaoAcaoModel';
import { TipoPermissaoAcao } from '../model/TipoPermissaoAcao';
import { Sequelize } from 'sequelize-typescript';
import { Organizacao } from '../model/OrganizacaoModel';
import { json } from 'body-parser';

class AuthTokenService{
    constructor(){
    }

    public async getLoginAuth(req, res){
        let data = new Data()
        try {
            let login = req.params["login"]
            let result = await Colaborador.findOne({where:{"login": login}})
            let useAuth= {}
            if(!result){
                data.addMsg(tMsg.DANGER, msgAuth.loginInvalido)
            }else{
                useAuth["login"] = result["login"]
                useAuth["nome"] = result["nome"]
            }
            data.obj = useAuth
        } catch (error) {
            data.addMsg(tMsg.DANGER,  msgAuth.erroAoConsultarLoginDeAcesso)
        }finally{
            res.status(200).json(data)
        }
    }

    public async authenticate(req, res){
        let data = new Data()
        //let result: any
        let result: {[k: string]: any} = {}
        let perfisValidacao: {[k: string]: any} = {}

        try {
            //let userLogin = req.body;
            let login = req.body.login
            let password = req.body.password

            result.colaborador = await Colaborador.findOne( 
                { where: { "login" : login, "status" : 2 } ,
                attributes: { exclude: [ "dataCadastro", "dataAtualizacao" ] } } )
            
            if (!result || !UtilService.isPasswordCorrect(result.colaborador['password'], password)) {
                res.sendStatus(401);
            }

            perfisValidacao = await FilialPerfil.findOne (
                { where: { "colaboradorId" : result.colaborador.id, "padrao" : 1 },
                attributes: { exclude: [ "id", "filialId", "colaboradorId", "diasFolga", "padrao" ] } } 
            )

             await Acao.sequelize.query('SELECT * FROM acao WHERE ID IN (SELECT ACAO_ID FROM perfil_acao WHERE PERFIL_ID in ( select perfil_id from filial_perfil where colaborador_id = :IDCOLABORADOR ) )', 
                 { replacements : { IDCOLABORADOR : result.colaborador.id } ,  model: Acao }).then( acoesList => {
                     //console.log("Ações Encontradas => " + acoesList)
                     result.acoes = acoesList
                 })
   
            if(!result){
                res.sendStatus(401);
            }else{
                result.colaborador['password'] = ''
                let dataBase = Date.now()
                let token = jwt.sign(
                    {   iss : 'https://sgf.mumps.com.br/', // (Issuer) Origem do token
                        iat: Math.floor(dataBase), // (issueAt) Timestamp de quando o token foi gerado
                        exp : Math.floor(dataBase + ( 1 * 60 * 60 * 1000 ) ), //(Expiration) Timestamp de quando o token expira
                        sub : result.colaborador.id, //(Subject) Entidade a quem o token pertence
                        login: result.colaborador.login,
                        validacao: perfisValidacao
                    }, secretToken.SECRET);
                res.append(secretToken.TOKEN, token)
                result[secretToken.TOKEN] = token
                data.obj = result
            }
            res.json(data)
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAuth.erroAoConsultarLoginDeAcesso)
            res.sendStatus(401).json(data)
        }
    }

    public async validateToken(req, res, next){
        let data = new Data()
        try {
            let token = req.body[secretToken.TOKEN] || req.query[secretToken.TOKEN] || req.headers[secretToken.TOKEN]
            if(token){
                let userLogged = jwt.verify(token, secretToken.SECRET)

                let colaborador = await Colaborador.findOne( 
                    { where: { "login" : userLogged["login"],  "status" : 2 } ,
                    attributes: { exclude: [ "password", "dataCadastro", "dataAtualizacao" ] } 
                })
                let urlOriginal = data.getUrlPadao("/api/v1", req.originalUrl);
                
                if (!colaborador.isAdmin) {

                    let isPermitido = await PerfilAcao.findOne(
                        { where : { "perfilId" : userLogged["validacao"].perfilId } ,
                        include : [ { model : Acao, as : 'acao' , where : { "url" : urlOriginal , "metodo" : req.method } } ]
                    })

                    if (!isPermitido) {
                        data.addMsg(tMsg.DANGER, msgAuth.acessoNegado)
                        res.status(401).json(data)
                        return
                    }

                }

                req["login"] = userLogged;
                next();
            }else{
                data.addMsg(tMsg.DANGER, msgAuth.tokenNaoEnviado)
                res.status(401).json(data)
                return
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAuth.tokenInvalido)
            data.addMsg(tMsg.STRACE, error)
            res.status(500).json(data)
            return
        }    
        
    }

    public async getUserLogged(req, res){
        let data = new Data()
        try {
            let userLogged = req["login"]
            if(userLogged){
                   data.obj = await Colaborador.findOne({where:{"login": userLogged.login}})
            }else{
                data.addMsg(tMsg.DANGER, msgAuth.nenhumLoginEncontrado)
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAuth.erroAoConsultarLoginDeAcesso)
            res.status(500).json(data)
        } finally {
            res.status(200).json(data)
        }
    }
}

export default new AuthTokenService()