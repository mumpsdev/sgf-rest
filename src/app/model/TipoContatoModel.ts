import {
    Table,
    Column,
    Model
} from "sequelize-typescript";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { Contato } from "./ContatoModel";
import { Default } from "sequelize-typescript/lib/annotations/Default";

@Table({tableName:"tipo_contato"})
export class TipoContato extends Model<TipoContato>{
    @Column({allowNull: false, field: "nome",})
    nome: string

    @Column({
        type : DataType.ENUM,
        values: ['string', 'number', 'boolean', 'date'],
        allowNull: false, 
        field: "tipo"
    })
    tipo: string

    @Column({allowNull: true, field: "mask", comment : 'Mascara do campo.'})
    mask: string

    @Column({allowNull: true, field: "regex", comment : 'Regex para aplicar na validação.'})
    regex: string

    @Column({allowNull: true, field: "qt_min_caracteres", comment : 'Quantidade de caracteres minimo.'})
    qtMinCaracters: number

    @Column({allowNull: true, field: "qt_max_caracteres", comment : 'Quantidade de caracteres maximo.'})
    qtMaxCaracters: number

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro"})
    dataCadastro: Date
    
    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao"})
    dataAtualizacao: Date

    @Default(true)
    @Column({allowNull: false, field: "ativo", comment : 'Informa se está ativo ou não.'})
    ativo: boolean    

    public static getAttributes() : string[]  {
        return ['id', 'nome', 'tipo', 'mask', 'regex', 'qtMinCaracters', 'qtMaxCaracters',
            'dataCadastro', 'dataAtualizacao', 'ativo'];
    } 
}