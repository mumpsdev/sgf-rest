import { TipoContato } from './TipoContatoModel'
import { TipoDocumento } from './TipoDocumentoModel'
import {
    Table,
    Column,
    Model,
    BelongsTo,
    Default
} from "sequelize-typescript";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Filial } from './FilialModel';
import { CreatedAt } from 'sequelize-typescript/lib/annotations/CreatedAt';
import { UpdatedAt } from 'sequelize-typescript/lib/annotations/UpdatedAt';
import { DataType } from 'sequelize-typescript/lib/enums/DataType';
import { FilialPerfil } from './FilialPerfilModel';
import { HasMany } from 'sequelize-typescript/lib/annotations/association/HasMany';

@Table({tableName:"colaborador"})
export class Colaborador extends Model<Colaborador>{
    
    @Column({allowNull: false, field: "nome", comment : 'Nome do Colaborador.'})
    nome: string

    @Column({allowNull: false, field: "login", comment : 'Login de acesso Web.'})
    login: string

    @Column({allowNull: false, field: "password", comment : 'Password do Colaborador.'})
    password: string
    //Flag que define super usuário.
    @Column({defaultValue: false, field: "is_admin", comment : 'Informa se o colaborador é administrator do sitema.'})
    isAdmin: boolean

    @Column({field: "dias_folga", comment : 'Dias de folga fixa do colaborador.'})
    diasFolga: string

    @Column({allowNull: false, field: "chave_grupo_documento", comment : 'Id da chave grupo que esta na tabela de documentos.'})
    chaveGrupoDocumento: number

    @Column({allowNull: false, field: "chave_grupo_contato", comment : 'Id da chave grupo que esta na tabela de contatos'})
    chaveGrupoContato: number

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro", comment : 'Data do cadastro do colaborador.'})
    dataCadastro: Date

    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao", comment : 'Data da atualização do colaborador.'})
    dataAtualizacao: Date

    @Default('2')
    @Column({
        type : DataType.ENUM,
        values: ['1', '2', '3', '4'],
        allowNull: false, 
        field: "status", 
        comment : '1 -> Pendente, 2 -> ativo, 3 -> suspenso e 4 -> removido'
    })
    status: number

    @HasMany( () => FilialPerfil)
    filialPerfil : FilialPerfil[]

    public static getAttributes() : string[]  {
        return ['id', 'nome', 'login', 'isAdmin', 'diasFolga', 'chaveGrupoDocumento', 'chaveGrupoContato', 'dataCadastro', 'dataAtualizacao', 'status'];
    } 
}