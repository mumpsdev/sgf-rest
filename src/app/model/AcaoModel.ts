import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { PerfilAcao } from "./PerfilAcaoModel";
import { PermissaoAcao } from "./PermissaoAcaoModel";
import { DataType } from "sequelize-typescript/lib/enums/DataType";

@Table({tableName:"acao"})
export class Acao extends Model<Acao>{
    static sequelize: any;

    @Column({allowNull: true, field: "nome", comment : 'Nome da ação.', defaultValue: null})
    nome: string

    @Column({allowNull: false, field: "url", comment : 'Url da ação.'})
    url: string

    @Column({allowNull: true, field: "id_no", comment : 'Id do agrupamento.', defaultValue: null})
    idNo: number

    @Column({allowNull: false, field: "item", comment : 'Item da url.'})
    item: string

    @Column({
        type : DataType.ENUM,
        values: [ 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH', 'NONE' ],
        allowNull: false, 
        field: "metodo"
    })
    metodo: string

    @Column({allowNull: true, field: "rota", comment : 'Rota para ser ultilizado no front-end', defaultValue: null})
    rota: string

    @Column({allowNull: false, field: "isMenu", comment : 'Informa se a ação é um menu ou uma request.'})
    isMenu: boolean

    @HasMany( () => PerfilAcao)
    perfilAcao : PerfilAcao[]
}