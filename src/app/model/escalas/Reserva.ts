import { Table, Model, Column, CreatedAt, UpdatedAt, DataType, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Semana } from "./Semana";
import { Colaborador } from "../ColaboradorModel";

@Table({tableName:"reserva"})
export class Reserva extends Model<Reserva>{

    @ForeignKey(() => Semana)
    @Column({field: "semana_id", comment : 'Semana.'})
    semanaId: number

    @BelongsTo(() => Semana)
    semana: Semana

    @ForeignKey(() => Colaborador)
    @Column({field: "colaborador_id", comment : 'Colaborador na escala.'})
    colaboradorId: number

    @BelongsTo(() => Colaborador)
    colaborador: Colaborador

}