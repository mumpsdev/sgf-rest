import { Table, Model, Column, CreatedAt, UpdatedAt, DataType, ForeignKey, BelongsTo, HasMany } from "sequelize-typescript";
import { MesAno } from "./MesAno";
import { Dia } from "./Dia";
import { Reserva } from "./Reserva";

@Table({tableName:"semana"})
export class Semana extends Model<Semana>{

    @Column({allowNull: false, field: "label", comment : 'Nome de apresetação do registro.'})
    label: string

    @Column({allowNull: false, field: "num_semana_ano", comment : 'Numero da semana em relação ao ano.'})
    numSemanaAno: number

    @ForeignKey(() => MesAno)
    @Column({field: "mes_ano_id", comment : 'Mes/ano da semana.'})
    MesAnoId: number

    @BelongsTo(() => MesAno)
    mesAno: MesAno

    @HasMany( () => Dia)
    dia : Dia[]

    @HasMany( () => Reserva)
    reserva : Reserva[]

}