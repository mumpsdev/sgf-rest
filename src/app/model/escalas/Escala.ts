import { Table, Model, Column, CreatedAt, UpdatedAt, DataType, ForeignKey, BelongsTo, HasMany } from "sequelize-typescript";
import { Dia } from "./Dia";
import { Colaborador } from "../ColaboradorModel";

@Table({tableName:"escala"})
export class Escala extends Model<Escala>{

    @ForeignKey(() => Dia)
    @Column({field: "dia_id", comment : 'Dia.'})
    diaId: number

    @BelongsTo(() => Dia)
    dia: Dia

    @ForeignKey(() => Colaborador)
    @Column({field: "colaborador_id", comment : 'Colaborador na escala.'})
    colaboradorId: number

    @BelongsTo(() => Colaborador)
    colaborador: Colaborador

    @Column({
        type : DataType.ENUM,
        values: ['1', '2'],
        allowNull: false, 
        field: "tipo_escala", 
        comment : '1 -> Trabalhando, 2 -> Folgando'
    })
    tipoEscala: number

}