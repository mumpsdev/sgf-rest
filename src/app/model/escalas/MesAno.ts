import { Table, Model, Column, CreatedAt, UpdatedAt, DataType, HasMany, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Semana } from "./Semana";
import { Filial } from "../FilialModel";

@Table({tableName:"mes_ano"})
export class MesAno extends Model<MesAno>{
    static sequelize: any;

    @Column({allowNull: false, field: "label", comment : 'Nome de apresetação do registro.'})
    label: string

    @Column({allowNull: false, field: "mes", comment : 'Numero do mes. em formato Ex. XX'})
    mes: number

    @Column({allowNull: false, field: "ano", comment : 'Data da atualização do colaborador.'})
    ano: number

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro", comment : 'Data do cadastro do colaborador.'})
    dataCadastro: Date

    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao", comment : 'Data da atualização do colaborador.'})
    dataAtualizacao: Date

    @Column({
        type : DataType.ENUM,
        values: ['1', '2', '3', '4', '5'],
        allowNull: false, 
        field: "status", 
        comment : '1 -> Aberto, 2 -> Liberado, 3 -> Fechado, 4 -> Cancelado e 5 -> Removido'
    })
    status: number

    @ForeignKey(() => Filial)
    @Column({field: "filial_id", comment : 'Filial da escala.'})
    filialId: number

    @BelongsTo(() => Filial)
    filial: Filial

    @HasMany( () => Semana)
    semana : Semana[]

}

//https://www.epochconverter.com/pt/semanas/2017