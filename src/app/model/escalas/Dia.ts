import { Table, Model, Column, CreatedAt, UpdatedAt, DataType, ForeignKey, BelongsTo, HasMany } from "sequelize-typescript";
import { Semana } from "./Semana";
import { Escala } from "./Escala";

@Table({tableName:"dia"})
export class Dia extends Model<Dia>{

    @Column({allowNull: false, field: "num_dia_ano", comment : 'Numero do dia no ano.'})
    numDiaAno: number

    @Column({allowNull: false, field: "num_dia_semana", comment : 'Numero do dia na semana.'})
    numDiaSemana: number

    @ForeignKey(() => Semana)
    @Column({field: "semana_id", comment : 'Semana.'})
    semanaId: number

    @BelongsTo(() => Semana)
    semana: Semana

    @HasMany( () => Escala)
    escala : Escala[]

}