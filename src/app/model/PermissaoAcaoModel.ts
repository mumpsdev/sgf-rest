import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { Acao } from "./AcaoModel";
import { HasOne } from "sequelize-typescript/lib/annotations/association/HasOne";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { TipoPermissaoAcao } from "./TipoPermissaoAcao";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";

@Table({tableName:"permissao_acao"})
export class PermissaoAcao extends Model<PermissaoAcao>{


    @ForeignKey(() => Acao)
    @Column({field: "acao_id", comment : 'Ação.'})
    acaoId: number

    @BelongsTo(() => Acao)
    acao: Acao

    @ForeignKey(() => TipoPermissaoAcao)
    @Column({field: "tipo_permissao_acao_id", comment : 'Tipo de Permissão para Ação.'})
    tipoPermissaoAcaoId: number

    @BelongsTo(() => TipoPermissaoAcao)
    tipoPermissaoAcao: TipoPermissaoAcao


}