import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Filial } from "./FilialModel";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";

@Table({tableName:"feriado"})
export class Feriado extends Model<Feriado>{

    @Column({allowNull: false, field: "dia_ano", comment : 'Dia do ano do feriado.'})
    diaAno: number

    @Column({allowNull: false, field: "dia_util", comment : 'informa se o dia é util.'})
    diaUtil: boolean

    @ForeignKey(() => Filial)
    @Column({field: "filial_id", comment : 'Filial selecionada para o feriado.'})
    filialId: number

    @BelongsTo(() => Filial)
    filial: Filial

    public static getAttributes() : string[]  {
        return ['id', 'diaAno', 'diaUtil', 'filialId'];
    } 
}