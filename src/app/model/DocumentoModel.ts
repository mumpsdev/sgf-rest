import {
    Table,
    Column,
    Model
} from "sequelize-typescript";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { TipoContato } from "./TipoContatoModel";
import { TipoDocumento } from "./TipoDocumentoModel";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { Default } from "sequelize-typescript/lib/annotations/Default";

@Table({tableName:"documento"})
export class Documento extends Model<Documento>{
    
    @Column({allowNull: false, field: "valor"})
    valor: string

    @ForeignKey(() => TipoDocumento)
    @Column({field: "tipo_documento_id"})
    tipoDocumentoId: number

    @BelongsTo(() => TipoDocumento)
    tipoDocumento: TipoDocumento

    @Column({field: "chave_grupo", allowNull: true})
    chaveGrupo: number

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro"})
    dataCadastro: Date
    
    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao"})
    dataAtualizacao: Date

    @Default(true)
    @Column({allowNull: false, field: "ativo", comment : 'Informa se está ativo ou não.'})
    ativo: boolean

    public static getAttributes() : string[]  {
        return ['id', 'valor', 'tipoDocumentoId', 'chaveGrupo', 'dataCadastro', 'dataAtualizacao', 'ativo'];
    }
}