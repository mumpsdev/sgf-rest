import {
    Table,
    Column,
    Model
} from "sequelize-typescript"

import { PrimaryKey } from "sequelize-typescript/lib/annotations/PrimaryKey";
import { AutoIncrement } from "sequelize-typescript/lib/annotations/AutoIncrement";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { TipoDocumento } from "./TipoDocumentoModel";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { Filial } from "./FilialModel";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { Default } from "sequelize-typescript/lib/annotations/Default";

@Table({tableName:"organizacao"})
export class Organizacao extends Model<Organizacao>{

    @Column({allowNull: false, field: "fantasia", comment : 'Nome fantasia da organização.'})
    fantasia: string
    
    @Column({allowNull: false, field: "razao_social", comment : 'Razão Social da organização.'})
    razaoSocial: string

    @Column({allowNull: false, field: "chave_grupo_contato", comment : 'Id da chave grupo que esta na tabela de contatos'})
    chaveGrupoContato: number
    
    @Column({allowNull: false, field: "chave_grupo_documento", comment : 'Id da chave grupo que esta na tabela de documentos.'})
    chaveGrupoDocumento: number

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro", comment : 'Data do cadastro da organização .'})
    dataCadastro: Date

    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao", comment : 'Data da atualização da organização.'})
    dataAtualizacao: Date

    @Default(2)
    @Column({
        type : DataType.ENUM,
        values: ['1', '2', '3', '4'],
        allowNull: false, 
        field: "status", 
        comment : '1 -> Pendente, 2 -> ativo, 3 -> suspenso e 4 -> removido'
    })
    status: number

    @HasMany( () => Filial)
    filiais : Filial[]

    public static getAttributes() : string[]  {
        return ['id', 'fantasia', 'razaoSocial', 'chaveGrupoContato', 'chaveGrupoDocumento', 'dataCadastro', 'dataAtualizacao', 'status'];
    }
}