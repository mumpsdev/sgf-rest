import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Perfil } from "./PerfilModel";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { Filial } from "./FilialModel";
import { Colaborador } from "./ColaboradorModel";

@Table({tableName:"filial_perfil"})
export class FilialPerfil extends Model<FilialPerfil>{
    static sequelize: any;

    @ForeignKey(() => Filial)
    @Column({field: "Filial_id", comment : 'Filial selecionada.'})
    filialId: number

    @BelongsTo(() => Filial)
    filial: Filial

    @ForeignKey(() => Perfil)
    @Column({field: "perfil_id", comment : 'Perfil selecionado.'})
    perfilId: number

    @BelongsTo(() => Perfil)
    perfil: Perfil

    @ForeignKey(() => Colaborador)
    @Column({field: "colaborador_id", comment : 'Colaborador selecionado.'})
    colaboradorId: number

    @BelongsTo(() => Colaborador)
    colaborador: Colaborador

    @Column({field: "dias_folga", comment : 'Dias de folga do Perfil vinculado a filial.'})
    diasFolga: string

    @Column({field: "padrao", comment : 'Filial padrão do colaborador.'})
    padrao: boolean

}