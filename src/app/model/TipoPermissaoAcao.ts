import { PermissaoAcao } from "./PermissaoAcaoModel";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { Model } from "sequelize-typescript/lib/models/Model";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Column } from "sequelize-typescript/lib/annotations/Column";

@Table({tableName:"tipo_permissao_acao"})
export class TipoPermissaoAcao extends Model<TipoPermissaoAcao>{

    @Column({allowNull: false, field: "nome", comment : 'Nome.'})
    nome: string

    @Column({allowNull: false, field: "descricao", comment : 'Nome.'})
    descricao: string

    @Column({allowNull: false, field: "url", comment : 'Nome.'})
    url: string

    @HasMany( () => PermissaoAcao)
    permissaoAcao : PermissaoAcao[]
}