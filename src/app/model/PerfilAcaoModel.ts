import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { Model } from "sequelize-typescript/lib/models/Model";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { Perfil } from "./PerfilModel";
import { Acao } from "./AcaoModel";

@Table({tableName:"perfil_acao"})
export class PerfilAcao extends Model<PerfilAcao>{

    @ForeignKey(() => Perfil)
    @Column({allowNull: false, field: "perfil_id", comment : 'Perfil selecionado.'})
    perfilId: number

    @BelongsTo(() => Perfil)
    perfil: Perfil

    @ForeignKey(() => Acao)
    @Column({allowNull: false, field: "acao_id", comment : 'Perfil selecionado.'})
    acaoId: number

    @BelongsTo(() => Acao)
    acao: Acao
}