import {
    Table,
    Column,
    Model
} from "sequelize-typescript"
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { Colaborador } from "./ColaboradorModel";
import { PrimaryKey } from "sequelize-typescript/lib/annotations/PrimaryKey";
import { AutoIncrement } from "sequelize-typescript/lib/annotations/AutoIncrement";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { Documento } from "./DocumentoModel";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { Default } from "sequelize-typescript/lib/annotations/Default";

@Table({tableName:"tipo_documento"})
export class TipoDocumento extends Model<TipoDocumento>{

    @Column({allowNull: false, field: "nome", comment : 'Nome do tipo de documento.'})
    nome: string

    @Column({
        type : DataType.ENUM,
        values: ['string', 'number', 'boolean', 'date'],
        allowNull: false, 
        field: "tipo"
    })
    tipo: string

    @Column({allowNull: true, field: "mask", comment : 'Mascara do campo.'})
    mask: string

    @Column({allowNull: true, field: "regex", comment : 'Regex para aplicar na validação.'})
    regex: string

    @Column({allowNull: true, field: "qt_min_caracteres", comment : 'Quantidade de caracteres minimo.'})
    qtMinCaracters: number

    @Column({allowNull: true, field: "qt_max_caracteres", comment : 'Quantidade de caracteres maximo.'})
    qtMaxCaracters: number
    
    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro", comment : 'Data do cadastro do tipo de documento.'})
    dataCadastro: Date
    
    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao", comment : 'Data da atualização do tipo de documento.'})
    dataAtualizacao: Date

    @Default(true)
    @Column({allowNull: false, field: "ativo", comment : 'Informa se está ativo ou não.'})
    ativo: boolean
    
    public static getAttributes() : string[]  {
        return ['id', 'nome', 'tipo', 'mask', 'regex', 'qtMinCaracters', 'qtMaxCaracters',
            'dataCadastro', 'dataAtualizacao', 'ativo'];
    } 
}