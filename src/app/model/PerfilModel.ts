import { Model } from 'sequelize-typescript';
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { FilialPerfil } from "./FilialPerfilModel";
import { PerfilAcao } from "./PerfilAcaoModel";
import { Unique } from "sequelize-typescript/lib/annotations/Unique";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { Default } from 'sequelize-typescript/lib/annotations/Default';

@Table({tableName:"perfil"})
export class Perfil extends Model<Perfil> {

    @Unique
    @Column({allowNull: false, field: "nome", comment : 'Nome do perfil.'})
    nome: string

    @HasMany( () => FilialPerfil)
    filialPerfil : FilialPerfil[]

    @HasMany( () => PerfilAcao)
    perfilAcao : PerfilAcao[]

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro", comment : 'Data do cadastro da organização .'})
    dataCadastro: Date

    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao", comment : 'Data da atualização da organização.'})
    dataAtualizacao: Date

    @Default(true)
    @Column({allowNull: false, field: "ativo", comment : 'Informa se está ativo ou não.'})
    ativo: boolean

    public static getAttributes() : string[]  {
        return ['id', 'nome', 'dataCadastro', 'dataAtualizacao', 'ativo'];
    } 
}