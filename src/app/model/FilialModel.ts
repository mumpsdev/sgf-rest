import {
    Table,
    Column,
    Model,
    ForeignKey,
    BelongsTo
} from "sequelize-typescript"

import { Organizacao } from "./OrganizacaoModel";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { Feriado } from "./FeriadosModel";
import { FilialPerfil } from "./FilialPerfilModel";
import { Colaborador } from "./ColaboradorModel";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { Sequelize } from "sequelize-typescript/lib/models/Sequelize";
import { DataTypeEnum } from "sequelize";
import { HasOne } from "sequelize-typescript/lib/annotations/association/HasOne";
import { MesAno } from "./escalas/MesAno";
import { Default } from "sequelize-typescript/lib/annotations/Default";

@Table({tableName:"filial"})
export class Filial extends Model<Filial>{

    @Column({allowNull: false, field: "fantasia", comment : 'Nome fantasia da filial.'})
    fantasia: string
    
    @Column({allowNull: false, field: "razao_social", comment : 'Razão Social da filial.'})
    razaoSocial: string

    @Column({allowNull: false, field: "chave_grupo_contato", comment : 'Id da chave grupo que esta na tabela de contatos'})
    chaveGrupoContato: number

    @Column({allowNull: false, field: "chave_grupo_documento", comment : 'Id da chave grupo que esta na tabela de documentos.'})
    chaveGrupoDocumento: number

    @ForeignKey(() => Organizacao)
    @Column({field: "organizacao_id", comment : 'Organização selecionada para a filial.'})
    organizacaoId: number

    @BelongsTo(() => Organizacao)
    organizacao: Organizacao

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro", comment : 'Data do cadastro da filial.'})
    dataCadastro : Date

    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao", comment : 'Data da atualização da filial.'})
    dataAtualizacao : Date    

    @Default(2)
    @Column({
        type : DataType.ENUM,
        values: ['1', '2', '3', '4'],
        allowNull: false, 
        field: "status", 
        comment : '1 -> Pendente, 2 -> ativo, 3 -> suspenso e 4 -> removido'
    })
    
    status: number

    @HasMany( () => Feriado)
    feriado : Feriado[]

    @HasMany( () => FilialPerfil)
    filialPerfil : FilialPerfil[]

    @HasOne( () => MesAno )
    mesAno : MesAno

    public static getAttributes() : string[]  {
        return ['id', 'fantasia', 'razaoSocial', 'organizacaoId', 'chaveGrupoContato', 'chaveGrupoDocumento', 'dataCadastro', 'dataAtualizacao', 'status'];
    }
}