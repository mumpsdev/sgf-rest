import {
    Table,
    Column,
    Model,
    Default
} from "sequelize-typescript";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { TipoContato } from "./TipoContatoModel";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";

@Table({tableName:"contato"})
export class Contato extends Model<Contato> {
    
    @Column({allowNull: false, field: "valor",})
    valor: string

    @ForeignKey(() => TipoContato)
    @Column({field: "tipo_contato_id"})
    tipoContatoId: number

    @BelongsTo(() => TipoContato)
    tipoContato: TipoContato

    @Column({field: "chave_grupo", allowNull: true})
    chaveGrupo: number

    @CreatedAt
    @Column({allowNull: false, field: "data_cadastro"})
    dataCadastro: Date
    
    @UpdatedAt
    @Column({allowNull: false, field: "data_atualizacao"})
    dataAtualizacao: Date

    @Default(true)
    @Column({allowNull: false, field: "ativo", comment : 'Informa se está ativo ou não.'})
    ativo: boolean

    public static getAttributes() : string[]  {
        return ['id', 'valor', 'tipoContatoId', 'chaveGrupo', 'dataCadastro', 'dataAtualizacao', 'ativo'];
    } 
}