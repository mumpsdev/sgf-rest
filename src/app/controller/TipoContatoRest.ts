import { URLs } from './../../util/Values';
import  TipoContatoService from '../service/TipoContatoService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class TipoContatoRest {
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.TIPO_CONTATO)        
        .get(TipoContatoService.list)                
        .post(TipoContatoService.create);

        exp.route(URLs.TIPO_CONTATO_ID)
        .get(TipoContatoService.get)
        .put(TipoContatoService.update)
        .delete(TipoContatoService.delete);
    }
}

export default new TipoContatoRest();