import { URLs } from './../../util/Values';
import  TipoDocumentoService from '../service/TipoDocumentoService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class TipoDocumentoRest {
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.TIPO_DOCUMENTO)        
        .get(TipoDocumentoService.list)                
        .post(TipoDocumentoService.create);

        exp.route(URLs.TIPO_DOCUMENTO_ID)
        .get(TipoDocumentoService.get)
        .put(TipoDocumentoService.update)
        .delete(TipoDocumentoService.delete);
    }
}

export default new TipoDocumentoRest();