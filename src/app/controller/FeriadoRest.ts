import { URLs } from './../../util/Values';
import  FeriadoService from '../service/FeriadoService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class FeriadoRest {
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.FERIADO)        
         .get(FeriadoService.list)                
         .post(FeriadoService.create);

        exp.route(URLs.FERIADO_ID)
         .get(FeriadoService.get)
         .put(FeriadoService.update)
         .delete(FeriadoService.delete);
    }
}

export default new FeriadoRest();