import { URLs } from './../../util/Values';
import * as express from "express";
import EscalaService from '../service/EscalaService';

class EscalaRest{
    constructor(){} 

    public setRoutes(exp: express.Application){
        exp.get(URLs.LIST_ANOS_COM_ESCALA_FILIAL, EscalaService.AnosComEscalasResumo)
        exp.get(URLs.LIST_ESCALAS_ANO_FILIAL, EscalaService.escalasDoAnoResumo)
        exp.get(URLs.SHOW_ESCALA_MES, EscalaService.escalaDoMesCompleto)
        exp.post(URLs.INICIAR_ESCALA_MES, EscalaService.iniciarBaseEscala)
        exp.post(URLs.GERAR_ESCALA_MES, EscalaService.gerarEscalaMes)
        exp.put(URLs.ESCALA_MUDAR_TIPO_TRABALHO_DIA_COLABORADOR, EscalaService.mudarTipoTrabalhoColaborador)
        exp.put(URLs.ESCALA_MUDAR_STATUS, EscalaService.mudaStatusEscala)
    }
    
}

export default new EscalaRest();