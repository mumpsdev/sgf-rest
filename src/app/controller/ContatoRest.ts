import { URLs } from './../../util/Values';
import  ContatoService from '../service/ContatoService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class ContatoRest {
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.CONTATO)        
        .get(ContatoService.list)                
        .post(ContatoService.create);

        exp.route(URLs.CONTATO_ID)
        .get(ContatoService.get)
        .put(ContatoService.update)
        .delete(ContatoService.delete);
    }
}

export default new ContatoRest();