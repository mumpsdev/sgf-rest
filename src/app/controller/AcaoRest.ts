import { URLs } from './../../util/Values';
import * as express from "express";
import AcaoService from "../service/AcaoService";

class AcaoRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.ACAO)
        .get(AcaoService.list)
        .post(AcaoService.create);

        exp.route(URLs.ACAO_NOTIN)
        .get(AcaoService.listNotIn)
        
        exp.route(URLs.ACAO_ID)
        .get(AcaoService.getById)
        .delete(AcaoService.remove)
        .put(AcaoService.update);
    }
}

export default new AcaoRest();