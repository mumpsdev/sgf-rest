import { URLs } from './../../util/Values';
import DocumentoService from '../service/DocumentoService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class DocumentoRest {
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.DOCUMENTO)        
        .get(DocumentoService.list)                
        .post(DocumentoService.create);

        exp.route(URLs.DOCUMENTO_ID)
        .get(DocumentoService.get)
        .put(DocumentoService.update)
        .delete(DocumentoService.delete);
    }
}

export default new DocumentoRest();