import * as express from "express";
import FilterService from "../service/FilterService";

class FilterRest {
    constructor(){} 

    public setRoutes(exp: express.Application){
        exp.use("/", FilterService.filterPath);
    }
}

export default new FilterRest();