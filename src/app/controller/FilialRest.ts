import { URLs } from './../../util/Values';
import  FilialService from '../service/FilialService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class FilialRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.FILIAL)
         .get(FilialService.list)
         .post(FilialService.create);

        exp.route(URLs.FILIAL_ID)
         .get(FilialService.get)
         .put(FilialService.update)
         .delete(FilialService.delete);
    }
}

export default new  FilialRest();