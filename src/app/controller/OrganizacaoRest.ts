import { URLs } from './../../util/Values';
import  OrganizacaoService from '../service/OrganizacaoService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class OrganizacaoRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.ORGANIZACAO)
        .get(OrganizacaoService.list)
        .post(OrganizacaoService.create);        
        
        exp.route(URLs.ORGANIZACAO_ID)
        .get(OrganizacaoService.get)
        .delete(OrganizacaoService.delete)
        .put(OrganizacaoService.update);
    }
}

export default new OrganizacaoRest();