import { Colaborador } from './../model/ColaboradorModel';
import { URLs } from './../../util/Values';
import  ColaboradorService from '../service/ColaboradorService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class ColaboradorRest {
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.COLABORADOR)
         .get(ColaboradorService.list)
         .post(ColaboradorService.create);

        exp.route(URLs.COLABORADOR_ID)
         .get(ColaboradorService.get)    
         .put(ColaboradorService.update)
         .delete(ColaboradorService.delete);    

        exp.route(URLs.COLABORADOR_FILIAIS)
        .get(ColaboradorService.getFiliais);
    }
}

export default new  ColaboradorRest();