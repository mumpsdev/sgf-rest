import { URLs } from './../../util/Values';
import  PerfilService from '../service/PerfilService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class PerfilRest {
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.PERFIL)        
        .get(PerfilService.list)                
        .post(PerfilService.create);

        exp.route(URLs.PERFIL_ID)
        .get(PerfilService.get)
        .put(PerfilService.update)
        .delete(PerfilService.delete);
    }
}

export default new PerfilRest();